﻿using UnityEngine;

namespace ConstructionSystem
{
    public class TestCameraBuilder : MonoBehaviour
    {
        [SerializeField] private BaseBlock[] spawnBlocks = new BaseBlock[0];
        [SerializeField] private float sphereCastRadius = 0.1f, sphereCastDistance = 15f;
        [SerializeField] private LayerMask layermask = Physics.DefaultRaycastLayers;
        private int targetSpawnIndex, lastSpawnIndex;
        private IBuildingBlockPlacer placer;
        private IBuildingBlockCreator creator;

        private void LateUpdate()
        {
            UpdateTargetSpawnIndex();
            TryCreatePreviewBlock();

            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                if (placer.PreviewBlock == null)
                {
                    TryDeleteLookingAtBlock();
                    return;
                }

                placer.DeletePreviewBlock();
            }

            if (placer.PreviewBlock == null || !placer.HasValidBuildLocation)
            {
                return;
            }

            if (!Input.GetKeyDown(KeyCode.Mouse0))
            {
                return;
            }

            if (creator.TrySpawn(spawnBlocks[lastSpawnIndex], placer))
            {
                placer.DeletePreviewBlock();
            }
        }

        private void UpdateTargetSpawnIndex()
        {
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                if (++targetSpawnIndex >= spawnBlocks.Length)
                {
                    targetSpawnIndex = 0;
                }

                CreatePreviewBlock();
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                if (--targetSpawnIndex < 0)
                {
                    targetSpawnIndex = spawnBlocks.Length - 1;
                }

                CreatePreviewBlock();
            }
        }

        private void TryCreatePreviewBlock()
        {
            if (targetSpawnIndex == -1 || placer.PreviewBlock != null)
            {
                return;
            }

            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                CreatePreviewBlock();
            }
        }

        private void CreatePreviewBlock()
        {
            placer.DeletePreviewBlock();
            lastSpawnIndex = targetSpawnIndex;
            BlockPreviewer.Ins.CreatePreview(placer, GetBaseBlockFromIndex(targetSpawnIndex));
            placer.Cast();
        }

        private void TryDeleteLookingAtBlock()
        {
            var ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
            var buildingBlock = BlockPhysics.SphereCast(ray, sphereCastRadius, sphereCastDistance, layermask, QueryTriggerInteraction.Ignore, RaycastDistanceComparer.Instance);

            if (buildingBlock != null)
            {
                buildingBlock.Delete(true);
            }
        }

        private BaseBlock GetBaseBlockFromIndex(int index)
        {
            if (spawnBlocks.Length <= index || index < 0)
            {
                return null;
            }

            return spawnBlocks[index];
        }

        private void Awake()
        {
            creator = new DefaultBlockCreator();
            placer = new SphereCastBlockPlacer(transform, sphereCastRadius, sphereCastDistance, layermask);
        }
    }
}