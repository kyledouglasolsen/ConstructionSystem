﻿using System.Collections.Generic;
using ConstructionSystem;
using UnityEngine;

public class TestBlockLoading : MonoBehaviour
{
    [SerializeField] private int count = 1000;
    [SerializeField] private BaseBlock blockPrefab;

    private List<IBuildingBlock> blocks = new List<IBuildingBlock>();

    private void OnEnable()
    {
        for (var i = 0; i < count; ++i)
        {
            var instance = Instantiate(blockPrefab, Vector3.up * 0.5732467f + Vector3.up * (i * blockPrefab.Data.ColliderData.Size.y), Quaternion.identity);
            BlockFactory.Initialize(instance, 0u, instance.transform.position, instance.transform.rotation);
            blocks.Add(instance);
        }
    }

    private void OnDisable()
    {
        for (var i = 0; i < blocks.Count; ++i)
        {
            var block = blocks[i];
            
            if (block != null)
            {
                if (block.IsInitialized)
                {
                    block.Delete(true);
                }
            }
        }
        
        blocks.Clear();
    }
}