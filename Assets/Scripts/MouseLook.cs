﻿using UnityEngine;

namespace ConstructionSystem
{
    public class MouseLook : MonoBehaviour
    {
        [SerializeField] private float speed = 7.5f;
        private Vector3 targetPosition;
        private Vector3 targetAngles;

        public Vector3 GetPosition()
        {
            return targetPosition;
        }

        public Quaternion GetRotation()
        {
            return Quaternion.Euler(targetAngles);
        }

        private void Start()
        {
            targetPosition = transform.position;
            targetAngles = transform.eulerAngles;
        }

        private void Update()
        {
            UpdateTargets();

            transform.position = GetPosition();
            transform.rotation = GetRotation();
        }

        private void UpdateTargets()
        {
            var moveSpeed = Input.GetKey(KeyCode.LeftShift) ? speed * 2 : speed;
            var xVelocity = 0f;
            var zVelocity = 0f;

            if (Input.GetKey(KeyCode.W))
            {
                zVelocity += 1f;
            }

            if (Input.GetKey(KeyCode.A))
            {
                xVelocity -= 1f;
            }

            if (Input.GetKey(KeyCode.S))
            {
                zVelocity -= 1f;
            }

            if (Input.GetKey(KeyCode.D))
            {
                xVelocity += 1f;
            }

            var localVelocity = new Vector3(xVelocity, 0f, zVelocity).normalized;

            if (Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.E))
            {
                localVelocity.y += 0.5f;
            }

            if (Input.GetKey(KeyCode.C) || Input.GetKey(KeyCode.Q))
            {
                localVelocity.y -= 0.5f;
            }

            localVelocity.Normalize();

            targetPosition += transform.TransformDirection(localVelocity) * moveSpeed * Time.smoothDeltaTime;
            var mouseDelta = new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"), 0f);
            var rotateDelta = new Vector3(-mouseDelta.y, mouseDelta.x, 0f);
            targetAngles += rotateDelta;
        }
    }
}