﻿//#define PROFILE_TIMERS

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

#if PROFILE_TIMERS
using UnityEngine.Profiling;

#endif

public class Timed : Singleton<Timed>
{
    private static volatile bool paused;
    private static volatile float timeScale = 1f;
    private static volatile int currentFrame, currentFixedFrame;
    private static int callbacksCount, nextFramesCount, nextFixedFramesCount;

    private static readonly object EveryFrameLock = new object();
    private readonly List<EveryFrameCallback> everyFrameCallbacks = new List<EveryFrameCallback>(100);

    private static readonly object EveryFramePoolLock = new object();
    private readonly PooledQueue<EveryFrameCallback> everyFramePool = new PooledQueue<EveryFrameCallback>(() => new EveryFrameCallback(), 100);

    private static readonly object NextFrameCallbacksLock = new object();
    private readonly Queue<NextFrameCallback> nextFrameCallbacks = new Queue<NextFrameCallback>(100);

    private static readonly object NextFixedFrameCallbacksLock = new object();
    private readonly Queue<NextFrameCallback> nextFixedFrameCallbacks = new Queue<NextFrameCallback>(100);

    private static readonly object NextFramePoolLock = new object();
    private readonly PooledQueue<NextFrameCallback> nextFramePool = new PooledQueue<NextFrameCallback>(() => new NextFrameCallback(), 100);

    private static readonly object NextFixedFramePoolLock = new object();
    private readonly PooledQueue<NextFrameCallback> nextFixedFramePool = new PooledQueue<NextFrameCallback>(() => new NextFrameCallback(), 100);

    private readonly ConcurrentQueue<Action> callbacks = new ConcurrentQueue<Action>();

    public static async Task In(float seconds, Action callback, CancellationTokenSource source = null)
    {
        var ins = Ins;

        if (ins == null)
        {
            return;
        }

        try
        {
            var delayTask = Task.Delay(TimeSpan.FromSeconds(seconds * timeScale), source?.Token ?? new CancellationToken(false));
            await delayTask.ConfigureAwait(false);

            while (paused)
            {
                Thread.Sleep(1);
            }

            ins.callbacks.Enqueue(callback);
            Interlocked.Increment(ref callbacksCount);
        }
        catch (TaskCanceledException)
        {
            // Ignore
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }

    public static async Task Every(float seconds, Action callback, int repeatCount = -1, CancellationTokenSource source = null)
    {
        if (seconds <= 0f)
        {
            Debug.LogError("Unable to start Timed task with <= 0f timer!");
            return;
        }

        var ins = Ins;

        if (ins == null)
        {
            return;
        }

        try
        {
            while (true)
            {
                var delayTask = Task.Delay(TimeSpan.FromSeconds(seconds * timeScale), source?.Token ?? new CancellationToken(false));
                await delayTask.ConfigureAwait(false);

                while (paused)
                {
                    Thread.Sleep(1);
                }

                ins.callbacks.Enqueue(callback);
                Interlocked.Increment(ref callbacksCount);

                if (repeatCount == -1)
                {
                    continue;
                }

                if (--repeatCount == 0)
                {
                    break;
                }
            }
        }
        catch (TaskCanceledException)
        {
            // Ignore
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }

    public static EveryFrameStopper EveryFrame(Action callback, int repeatCount = -1)
    {
        var ins = Ins;

        if (ins == null)
        {
            return default(EveryFrameStopper);
        }

        EveryFrameCallback everyFrameCallback;

        lock (EveryFramePoolLock)
        {
            do
            {
                everyFrameCallback = ins.everyFramePool.Dequeue();
            } while (everyFrameCallback == null);
        }

        everyFrameCallback.Setup(callback, repeatCount);

        lock (EveryFrameLock)
        {
            ins.everyFrameCallbacks.Add(everyFrameCallback);
        }

        return new EveryFrameStopper(everyFrameCallback);
    }

    public static void NextFrame(Action callback)
    {
        var ins = Ins;

        if (ins == null)
        {
            return;
        }

        NextFrameCallback nextFrameCallback;

        lock (NextFrameCallbacksLock)
        {
            do
            {
                nextFrameCallback = ins.nextFramePool.Dequeue();
            } while (nextFrameCallback == null);
        }

        nextFrameCallback.Setup(callback, currentFrame);

        lock (NextFrameCallbacksLock)
        {
            ins.nextFrameCallbacks.Enqueue(nextFrameCallback);
        }

        Interlocked.Increment(ref nextFramesCount);
    }

    public static void NextFixedFrame(Action callback)
    {
        var ins = Ins;

        if (ins == null)
        {
            return;
        }

        NextFrameCallback nextFrameCallback;

        lock (NextFixedFramePoolLock)
        {
            do
            {
                nextFrameCallback = ins.nextFixedFramePool.Dequeue();
            } while (nextFrameCallback == null);
        }

        nextFrameCallback.Setup(callback, currentFixedFrame);

        lock (NextFixedFrameCallbacksLock)
        {
            ins.nextFixedFrameCallbacks.Enqueue(nextFrameCallback);
        }

        Interlocked.Increment(ref nextFixedFramesCount);
    }

    private void FixedUpdate()
    {
        var failedExecuteCount = 0;

        if (nextFixedFramesCount > 0)
        {
            lock (NextFixedFrameCallbacksLock)
            {
                while (nextFixedFramesCount > 0)
                {
                    try
                    {
                        var nextFrameCallback = nextFixedFrameCallbacks.Dequeue();

                        if (nextFrameCallback == null)
                        {
                            continue;
                        }

                        if (nextFrameCallback.Execute(currentFixedFrame))
                        {
                            lock (NextFixedFramePoolLock)
                            {
                                nextFixedFramePool.Enqueue(nextFrameCallback);
                            }
                        }
                        else
                        {
                            nextFixedFrameCallbacks.Enqueue(nextFrameCallback);
                            ++failedExecuteCount;
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                    }
                    finally
                    {
                        Interlocked.Decrement(ref nextFixedFramesCount);
                    }
                }
            }
        }

        Interlocked.Add(ref nextFixedFramesCount, failedExecuteCount);

        ++currentFixedFrame;
    }

    private void Update()
    {
        #region Callbacks

#if PROFILE_TIMERS
        Profiler.BeginSample($"Callbacks {callbacksCount}");
#endif

        while (callbacksCount > 0)
        {
            try
            {
#if PROFILE_TIMERS
                Profiler.BeginSample("TryDequeue");
#endif
                Action callback;

                if (!callbacks.TryDequeue(out callback) || callback == null)
                {
                    continue;
                }

#if PROFILE_TIMERS
                Profiler.EndSample();
#endif

#if PROFILE_TIMERS
                Profiler.BeginSample($"{callback.Method.DeclaringType}.{callback.Method.Name}");
#endif

                callback.Invoke();
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
            finally
            {
                Interlocked.Decrement(ref callbacksCount);
            }

#if PROFILE_TIMERS
            Profiler.EndSample();
#endif
        }
#if PROFILE_TIMERS
        Profiler.EndSample();
#endif

        #endregion Callbacks

        #region Next Frame Callbacks

#if PROFILE_TIMERS
        Profiler.BeginSample("Next Frame Callbacks");
#endif

        var failedExecuteCount = 0;

        if (nextFramesCount > 0)
        {
            lock (NextFrameCallbacksLock)
            {
                while (nextFramesCount > 0)
                {
                    try
                    {
                        var nextFrameCallback = nextFrameCallbacks.Dequeue();

                        if (nextFrameCallback == null)
                        {
                            continue;
                        }

                        if (nextFrameCallback.Execute(currentFrame))
                        {
                            lock (NextFramePoolLock)
                            {
                                nextFramePool.Enqueue(nextFrameCallback);
                            }
                        }
                        else
                        {
                            nextFrameCallbacks.Enqueue(nextFrameCallback);
                            ++failedExecuteCount;
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                    }
                    finally
                    {
                        Interlocked.Decrement(ref nextFramesCount);
                    }
                }
            }
        }

        Interlocked.Add(ref nextFramesCount, failedExecuteCount);

#if PROFILE_TIMERS
        Profiler.EndSample();
#endif

        #endregion Next Frame Callbacks

        #region Every Frame Callbacks

#if PROFILE_TIMERS
        Profiler.BeginSample("Every Frame Callbacks");
#endif

        lock (EveryFrameLock)
        {
            var everyFrameCallbackCount = everyFrameCallbacks.Count;

            for (var i = everyFrameCallbackCount - 1; i >= 0; --i)
            {
                var addToPool = false;
                EveryFrameCallback callback = null;

                try
                {
                    callback = everyFrameCallbacks[i];

                    if (callback == null)
                    {
                        continue;
                    }

                    if (!callback.Execute())
                    {
                        everyFrameCallbacks.RemoveAt(i);
                        addToPool = true;
                    }
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                    everyFrameCallbacks.RemoveAt(i);
                    addToPool = true;
                }
                finally
                {
                    if (addToPool)
                    {
                        lock (everyFramePool)
                        {
                            everyFramePool.Enqueue(callback);
                        }
                    }

                    --everyFrameCallbackCount;
                }
            }
        }

#if PROFILE_TIMERS
        Profiler.EndSample();
#endif

        #endregion Every Frame Callbacks

        timeScale = Time.timeScale;
        currentFrame = Time.frameCount;
    }

    private void OnApplicationPause(bool isPaused)
    {
        paused = isPaused;
    }

    protected override void OnAwake()
    {
        SetSingletonInstance(this);

        timeScale = Time.timeScale;
        currentFrame = Time.frameCount;
        currentFixedFrame = 0;

#if UNITY_EDITOR
        UnityEditor.EditorApplication.playmodeStateChanged += () => { paused = UnityEditor.EditorApplication.isPaused; };
#endif
    }

    private void Start()
    {
#if UNITY_EDITOR
        paused = UnityEditor.EditorApplication.isPaused;
#else
        paused = false;
#endif
    }

    public class EveryFrameCallback
    {
        public long Revision => revision;

        private long revision;
        private Action Callback { get; set; }
        private int RepeatCount { get; set; }
        private bool canceled;

        public void Setup(Action callback, int repeatCount = -1)
        {
            Callback = callback;
            RepeatCount = repeatCount;
            canceled = false;
            Interlocked.Increment(ref revision);
        }

        public bool Execute()
        {
            if (canceled)
            {
                return false;
            }

#if PROFILE_TIMERS
            Profiler.BeginSample($"{Callback.Method.DeclaringType}.{Callback.Method.Name}");
#endif

            Callback();

#if PROFILE_TIMERS
            Profiler.EndSample();
#endif

            return RepeatCount == -1 || --RepeatCount != 0;
        }

        public void Cancel()
        {
            canceled = true;
        }
    }

    public struct EveryFrameStopper
    {
        private long capturedRevision;
        private EveryFrameCallback callback;

        public EveryFrameStopper(EveryFrameCallback callback)
        {
            this.callback = callback;
            capturedRevision = callback.Revision;
        }

        public void Cancel()
        {
            if (callback == null || capturedRevision != callback.Revision)
            {
                return;
            }

            capturedRevision = 0;
            callback.Cancel();
        }
    }

    private class NextFrameCallback
    {
        public void Setup(Action action, int capturedFrame)
        {
            Callback = action;
            CapturedFrame = capturedFrame;
        }

        private Action Callback { get; set; }
        private int CapturedFrame { get; set; }

        public bool Execute(int currentFrame)
        {
            if (CapturedFrame == currentFrame)
            {
                return false;
            }

#if PROFILE_TIMERS
            Profiler.BeginSample($"{Callback.Method.DeclaringType}.{Callback.Method.Name}");
#endif

            Callback();

#if PROFILE_TIMERS
            Profiler.EndSample();
#endif
            return true;
        }
    }
}