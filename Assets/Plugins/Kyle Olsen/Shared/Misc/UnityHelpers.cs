﻿using UnityEngine;

public static class UnityHelpers
{
    public static bool IsNull(object target)
    {
        if (target == null)
        {
            return true;
        }

        var behaviour = target as Behaviour;
        return behaviour == null;
    }
}