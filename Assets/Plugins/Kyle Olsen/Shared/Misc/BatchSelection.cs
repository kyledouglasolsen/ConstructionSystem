﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEditor;

public static class BatchSelection
{
    [MenuItem("Tools/Batch Selected Meshes")]
    private static void BatchSelectedMeshes()
    {
        var lastPath = EditorPrefs.GetString("PreviousBatchedMeshSaveLocation", Application.dataPath);
        var lastName = EditorPrefs.GetString("PreviousBatchedMeshSaveName", "NewMesh");
        var savePath = EditorUtility.SaveFilePanel("Choose save location", lastPath, lastName, "asset");

        if (string.IsNullOrEmpty(savePath))
        {
            Debug.Log("Cancel mesh batch!");
            return;
        }

        var relativePath = FileUtil.GetProjectRelativePath(savePath);

        if (string.IsNullOrEmpty(relativePath))
        {
            return;
        }

        savePath = relativePath;
        lastPath = Path.GetDirectoryName(savePath);
        lastName = Path.GetFileNameWithoutExtension(savePath);

        EditorPrefs.SetString("PreviousBatchedMeshSaveLocation", lastPath);
        EditorPrefs.SetString("PreviousBatchedMeshSaveName", lastName);

        var list = new List<MeshFilter>();

        foreach (var go in Selection.gameObjects)
        {
            list.AddRange(go.GetComponentsInChildren<MeshFilter>());
        }

        list = list.Distinct().ToList();

        if (list.Count < 1)
        {
            Debug.Log("Didn't find any selected meshes to batch!");
            return;
        }

        var combined = new CombineInstance[list.Count];

        for (var i = 0; i < combined.Length; ++i)
        {
            var parent = list[i].transform.parent;
            Matrix4x4 matrix;
            
            if (parent != null)
            {
                matrix = parent.worldToLocalMatrix * list[i].transform.localToWorldMatrix;
            }
            else
            {
                matrix = list[i].transform.localToWorldMatrix;
            }

            combined[i] = new CombineInstance() {mesh = list[i].sharedMesh, transform = matrix};
        }

        var mesh = new Mesh {name = lastName};
        mesh.CombineMeshes(combined);

        var currentMesh = AssetDatabase.LoadAssetAtPath<Mesh>(savePath);
        if (currentMesh != null)
        {
            EditorUtility.CopySerialized(mesh, currentMesh);
            AssetDatabase.SaveAssets();
        }
        else
        {
            AssetDatabase.CreateAsset(mesh, savePath);
        }
    }
}