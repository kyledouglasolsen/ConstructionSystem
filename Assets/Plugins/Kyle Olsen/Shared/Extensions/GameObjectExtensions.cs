﻿using UnityEngine;

public static class GameObjectExtensions
{
    public static void SetLayerRecursive(this GameObject gameObject, int layer)
    {
        gameObject.layer = layer;

        foreach (Transform t in gameObject.transform)
        {
            SetLayerRecursive(t.gameObject, layer);
        }
    }
}