﻿using UnityEngine;
using System.Collections;

public static class MatrixExtensions
{
    public static void ApplyToTransform(this Matrix4x4 matrix, Transform transform)
    {
        transform.localPosition = GetPosition(matrix);
        transform.localRotation = GetRotation(matrix);
        transform.localScale = GetScale(matrix);
    }

    public static Quaternion GetRotation(this Matrix4x4 matrix)
    {
        var f = matrix.GetColumn(2);
        return f == Vector4.zero ? Quaternion.identity : Quaternion.LookRotation(f, matrix.GetColumn(1));
    }

    public static Vector3 GetPosition(this Matrix4x4 matrix)
    {
        return matrix.GetColumn(3);
    }

    public static Vector3 GetScale(this Matrix4x4 matrix)
    {
        return new Vector3(matrix.GetColumn(0).magnitude, matrix.GetColumn(1).magnitude, matrix.GetColumn(2).magnitude);
    }
}