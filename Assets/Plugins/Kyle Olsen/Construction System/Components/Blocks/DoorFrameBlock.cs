﻿using System;
using ConstructionSystem.BuildingSockets;

namespace ConstructionSystem
{
    public class DoorFrameBlock : WallBlock
    {
        public override Type GetTargetSocketType()
        {
            return typeof(DoorFrameSocket);
        }
    }
}