﻿using System;
using ConstructionSystem.BuildingSockets;
using ConstructionSystem.Conditions;
using UnityEngine;

namespace ConstructionSystem
{
    public abstract partial class BaseBlock : ConstructionBehaviour, IBuildingBlock
    {
        [SerializeField] private BaseBlockData blockData = null;
        [SerializeField] private SocketCollection sockets = null;
        [SerializeField] private ConditionCollection conditions = null;

        public IBuildingBlockData Data => blockData;
        public ISocketCollection Sockets => sockets;
        public IConditionCollection Conditions => conditions;

        public bool IsInitialized { get; private set; }
        public bool IsPreview { get; set; }

        public IBuildingStructure Structure { get; set; }
        public IConstructionCollider Collider { get; private set; }
        public IActiveSocketCollection ActiveSockets { get; private set; }
        public event Action<IBuildingBlock> InitializeEvent = delegate { };
        public event Action<IBuildingBlock> DeleteEvent = delegate { };

        public Bounds Bounds { get; set; }

        public void Initialize(bool isPreview)
        {
            IsPreview = isPreview;
            gameObject.layer = ConstructionLayers.BuildingLayer;
            
            if (ActiveSockets == null)
            {
                ActiveSockets = new ActiveSocketCollection(this, Sockets, isPreview);
            }

            CreateCollider();

            IsInitialized = true;
            InitializeEvent(this);
        }

        public void SetStructure(IBuildingStructure structure)
        {
            Structure = structure;
        }

        public void SetCollider(IConstructionCollider constructionCollider)
        {
            Collider = constructionCollider;
            OnSetCollider();
        }

        public bool EvaluateConditions()
        {
            IConstructionCondition failedCondition;
            return Conditions.Evaluate(this, out failedCondition);
        }

        public bool EvaluateConditions(out IConstructionCondition failedCondition)
        {
            return Conditions.Evaluate(this, out failedCondition);
        }

        public virtual void Delete(bool destroyGo)
        {
            Structure?.RemoveBlock(this);

            DeleteEvent(this);
            IsInitialized = false;

            DestroyCollider();
            //ActiveSockets?.DeleteAll();

            if (destroyGo)
            {
                Destroy(gameObject);
            }
        }

        public void ForceUpdateBounds()
        {
            var worldSize = transform.TransformVector(Data.ColliderData.Size).Abs();
            Bounds = new Bounds(TransformPoint(Data.ColliderData.Center), worldSize);
        }

        public IBuildingBlock Copy()
        {
            return Instantiate(this, transform.position, transform.rotation);
        }

        public abstract Type GetTargetSocketType();

        public override void SetPositionAndRotation(Vector3 position, Quaternion rotation)
        {
            base.SetPositionAndRotation(position, rotation);
            ForceUpdateBounds();
        }

        protected virtual void CreateCollider()
        {
            if (!IsPreview)
            {
                BatchedWorldCollision.Add(this);
            }
        }

        protected virtual void DestroyCollider()
        {
            BatchedWorldCollision.Remove(this);
        }

        protected virtual void OnSetCollider()
        {
        }
    }
}