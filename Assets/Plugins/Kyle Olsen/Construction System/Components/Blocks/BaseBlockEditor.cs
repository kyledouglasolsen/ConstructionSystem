﻿using UnityEngine;

#if UNITY_EDITOR

namespace ConstructionSystem
{
    public abstract partial class BaseBlock
    {
        [SerializeField] private bool EDITOR_DrawGizmos = true;

        protected virtual void OnValidate()
        {
            if (Data == null || Conditions == null)
            {
                return;
            }

            foreach (var condition in Conditions)
            {
                condition.OnValidate();
            }
        }

        protected virtual void OnDrawGizmos()
        {
            if (!EDITOR_DrawGizmos || Data == null)
            {
                return;
            }

            if (IsPreview)
            {
                Gizmos.color = Color.magenta;
                Gizmos.DrawWireCube(Bounds.center, Bounds.size);
            }
            else if (this is IBatchCollision)
            {
                var batch = this as IBatchCollision;
                var blockBatch = batch?.BlockBatch;
                
                if (blockBatch != null)
                {
                    Gizmos.color = Color.green;
                    Gizmos.DrawWireCube(blockBatch.Bounds.center, blockBatch.Bounds.size);
                }
            }
            else
            {
                Gizmos.color = Color.red;
                Gizmos.DrawWireCube(Bounds.center, Bounds.size);
            }

            Data.DrawGizmos(this);

            if (Sockets != null)
            {
                foreach (var socket in Sockets)
                {
                    var color = Gizmos.color;
                    socket.DrawGizmos(this);
                    Gizmos.color = color;
                }
            }

            if (Conditions != null)
            {
                foreach (var condition in Conditions)
                {
                    var color = Gizmos.color;
                    condition.DrawGizmos(this);
                    Gizmos.color = color;
                }
            }
        }
    }
}

#endif