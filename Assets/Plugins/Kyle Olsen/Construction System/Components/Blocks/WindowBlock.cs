﻿using System;
using ConstructionSystem.BuildingSockets;

namespace ConstructionSystem
{
    public class WindowBlock : WallBlock
    {
        public override Type GetTargetSocketType()
        {
            return typeof(WallSocket);
        }
    }
}