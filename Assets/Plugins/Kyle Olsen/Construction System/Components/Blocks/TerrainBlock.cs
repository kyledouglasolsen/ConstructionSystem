﻿using System;
using ConstructionSystem.BuildingSockets;

namespace ConstructionSystem
{
    public class TerrainBlock : BaseBlock
    {
        public override void Delete(bool destroyGo)
        {
        }

        public override Type GetTargetSocketType()
        {
            return typeof(TerrainSocket);
        }
    }
}