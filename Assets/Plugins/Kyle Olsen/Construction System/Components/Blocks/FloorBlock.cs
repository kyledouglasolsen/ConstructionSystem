﻿using System;
using ConstructionSystem.BuildingSockets;

namespace ConstructionSystem
{
    public class FloorBlock : BaseBlock, IBatchCollision, IBlockPhysics
    {
        public bool IsBatched { get; private set; }
        public IBlockBatch<IBuildingBlock> BlockBatch => Collider as IBlockBatch<IBuildingBlock>;
        
        public event Action<IBlockPhysics> SimulatingPhysicsChangedEvent = delegate { };
        public bool SimulatingPhysics { get; private set; }

        public IBlockBatch<IBuildingBlock> CreateNewBatch()
        {
            return new CubeBlockBatch(this, Rotation);
        }

        public IBatchCollision FindNeighborBatch()
        {
            return Structure?.FindNeighborBatch<FloorBlock>(this);
        }

        public override Type GetTargetSocketType()
        {
            return typeof(FloorSocket);
        }

        protected override void OnSetCollider()
        {
            IsBatched = Collider is IBlockBatch<IBuildingBlock>;
        }

        public void SetSimulatingPhysics(bool newSimulatingPhysics)
        {
            if(SimulatingPhysics != newSimulatingPhysics)
            {
                SimulatingPhysics = newSimulatingPhysics;
                SimulatingPhysicsChangedEvent(this);
            }
        }
    }
}