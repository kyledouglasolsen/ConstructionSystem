﻿using System;
using ConstructionSystem.BuildingSockets;

namespace ConstructionSystem
{
    public class CubeWallBlock : WallBlock, IBatchCollision
    {
        public bool IsBatched { get; private set; }
        public IBlockBatch<IBuildingBlock> BlockBatch => Collider as IBlockBatch<IBuildingBlock>;

        public IBlockBatch<IBuildingBlock> CreateNewBatch()
        {
            return new CubeBlockBatch(this, Rotation);
        }

        public IBatchCollision FindNeighborBatch()
        {
            return Structure?.FindNeighborBatch<CubeWallBlock>(this);
        }

        public override Type GetTargetSocketType()
        {
            return typeof(WallSocket);
        }
        
        protected override void OnSetCollider()
        {
            IsBatched = Collider is IBlockBatch<IBuildingBlock>;
        }
    }
}