﻿using System;
using ConstructionSystem.BuildingSockets;

namespace ConstructionSystem
{
    public class SlopedWallBlock : WallBlock
    {
        public override Type GetTargetSocketType()
        {
            return typeof(WallSocket);
        }
    }
}