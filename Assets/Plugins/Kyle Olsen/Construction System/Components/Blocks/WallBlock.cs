﻿using System;

namespace ConstructionSystem
{
    public abstract class WallBlock : BaseBlock, IBlockPhysics
    {
        public event Action<IBlockPhysics> SimulatingPhysicsChangedEvent = delegate { };

        public bool SimulatingPhysics { get; private set; }

        public void SetSimulatingPhysics(bool newSimulatingPhysics)
        {
            if (SimulatingPhysics != newSimulatingPhysics)
            {
                SimulatingPhysics = newSimulatingPhysics;
                SimulatingPhysicsChangedEvent(this);
            }
        }
    }
}