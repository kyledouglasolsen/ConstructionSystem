﻿using UnityEngine;

namespace ConstructionSystem
{
    [CreateAssetMenu(fileName = InternalBlockDataName, menuName = ScriptableObjectPaths.BlockData + InternalBlockDataName, order = 0)]
    public class TerrainBlockData : BaseBlockData
    {
        private const string InternalBlockDataName = "Terrain Block Data";
        
        public override IColliderData ColliderData => null;
        protected override string BlockDataName => InternalBlockDataName;
    }
}