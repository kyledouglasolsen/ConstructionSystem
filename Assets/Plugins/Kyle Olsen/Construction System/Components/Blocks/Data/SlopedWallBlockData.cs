﻿using UnityEngine;

namespace ConstructionSystem
{
    [CreateAssetMenu(fileName = InternalBlockDataName, menuName = ScriptableObjectPaths.BlockData + InternalBlockDataName, order = 0)]
    public class SlopedWallBlockData : WallBlockData
    {
        private const string InternalBlockDataName = "Sloped Wall Block Data";

        [SerializeField] private MeshColliderData meshColliderData = new MeshColliderData();

        public override IColliderData ColliderData
        {
            get { return meshColliderData; }
        }

        protected override string BlockDataName
        {
            get { return InternalBlockDataName; }
        }
    }
}