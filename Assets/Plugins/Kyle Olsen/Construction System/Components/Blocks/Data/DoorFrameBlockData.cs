﻿using UnityEngine;

namespace ConstructionSystem
{
    [CreateAssetMenu(fileName = InternalBlockDataName, menuName = ScriptableObjectPaths.BlockData + InternalBlockDataName, order = 0)]
    public class DoorFrameBlockData : WallBlockData
    {
        private const string InternalBlockDataName = "Door Frame Block Data";

        [SerializeField] private MultiBoxColliderData multiBoxColliderData = new MultiBoxColliderData();

        public override IColliderData ColliderData
        {
            get { return multiBoxColliderData; }
        }

        protected override string BlockDataName
        {
            get { return InternalBlockDataName; }
        }
    }
}