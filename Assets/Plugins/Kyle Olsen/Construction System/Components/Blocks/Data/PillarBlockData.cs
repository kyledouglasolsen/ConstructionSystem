﻿using UnityEngine;

namespace ConstructionSystem
{
    [CreateAssetMenu(fileName = InternalBlockDataName, menuName = ScriptableObjectPaths.BlockData + InternalBlockDataName, order = 0)]
    public class PillarBlockData : BaseBlockData
    {
        private const string InternalBlockDataName = "Pillar Block Data";

        [SerializeField] private BoxColliderData boxColliderData = new BoxColliderData();

        public override IColliderData ColliderData => boxColliderData;
        protected override string BlockDataName => InternalBlockDataName;
    }
}