﻿using UnityEngine;

namespace ConstructionSystem
{
    public abstract class BaseBlockData : ScriptableObject, IBuildingBlockData
    {
#if UNITY_EDITOR
        [SerializeField] private bool drawGizmos = true;
        [SerializeField] private Color gizmoColor = new Color(0f, 0f, 1f, 0.2f);
#endif
        [SerializeField] private bool requireSnap = true;
        [SerializeField] private Vector3 buildPositionOffset = Vector3.zero;
        [SerializeField] private Vector3 buildRotationOffset = Vector3.zero;
        
        public bool RequireSnap => requireSnap;
        public Vector3 BuildPositionOffset => buildPositionOffset;
        public Vector3 BuildRotationOffset => buildRotationOffset;

        public abstract IColliderData ColliderData { get; }
        protected abstract string BlockDataName { get; }

#if UNITY_EDITOR
        public virtual void DrawGizmos(IBuildingBlock block)
        {
            if (!drawGizmos || ColliderData == null)
            {
                return;
            }

            Gizmos.color = gizmoColor;
            ColliderData.DrawGizmos(block, Vector3.zero, Vector3.zero, false);
        }
#endif
    }
}