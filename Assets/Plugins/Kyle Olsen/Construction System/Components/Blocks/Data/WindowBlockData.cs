﻿using UnityEngine;

namespace ConstructionSystem
{
    [CreateAssetMenu(fileName = InternalBlockDataName, menuName = ScriptableObjectPaths.BlockData + InternalBlockDataName, order = 0)]
    public class WindowBlockData : WallBlockData
    {
        private const string InternalBlockDataName = "Window Block Data";

        [SerializeField] private MultiBoxColliderData multiBoxColliderData = new MultiBoxColliderData();

        public override IColliderData ColliderData => multiBoxColliderData;
        protected override string BlockDataName => InternalBlockDataName;
    }
}