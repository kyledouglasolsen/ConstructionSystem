﻿using UnityEngine;

namespace ConstructionSystem
{
    [CreateAssetMenu(fileName = InternalBlockDataName, menuName = ScriptableObjectPaths.BlockData + InternalBlockDataName, order = 0)]
    public class DoorBlockData : BaseBlockData
    {
        private const string InternalBlockDataName = "Door Block Data";

        [SerializeField] private BoxColliderData boxColliderData = new BoxColliderData();

        public override IColliderData ColliderData => boxColliderData;
        protected override string BlockDataName => InternalBlockDataName;
    }
}