﻿using UnityEngine;

namespace ConstructionSystem
{
    [CreateAssetMenu(fileName = InternalBlockDataName, menuName = ScriptableObjectPaths.BlockData + InternalBlockDataName, order = 0)]
    public class StairsBlockData : BaseBlockData
    {
        private const string InternalBlockDataName = "Stairs Block Data";

        [SerializeField] private MeshColliderData meshColliderData = new MeshColliderData();

        public override IColliderData ColliderData => meshColliderData;
        protected override string BlockDataName => InternalBlockDataName;
    }
}