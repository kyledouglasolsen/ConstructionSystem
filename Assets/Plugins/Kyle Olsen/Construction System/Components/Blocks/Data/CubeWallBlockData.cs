﻿using UnityEngine;

namespace ConstructionSystem
{
    [CreateAssetMenu(fileName = InternalBlockDataName, menuName = ScriptableObjectPaths.BlockData + InternalBlockDataName, order = 0)]
    public class CubeWallBlockData : WallBlockData
    {
        private const string InternalBlockDataName = "Cube Wall Block Data";

        [SerializeField] private BoxColliderData boxColliderData = new BoxColliderData();

        public override IColliderData ColliderData => boxColliderData;
        protected override string BlockDataName => InternalBlockDataName;
    }
}