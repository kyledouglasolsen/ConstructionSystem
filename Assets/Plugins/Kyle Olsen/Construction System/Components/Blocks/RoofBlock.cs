﻿using System;
using ConstructionSystem.BuildingSockets;

namespace ConstructionSystem
{
    public class RoofBlock : BaseBlock, IBlockPhysics
    {
        public event Action<IBlockPhysics> SimulatingPhysicsChangedEvent = delegate { };
        public bool SimulatingPhysics { get; private set; }

        public void SetSimulatingPhysics(bool newSimulatingPhysics)
        {
            if (SimulatingPhysics != newSimulatingPhysics)
            {
                SimulatingPhysics = newSimulatingPhysics;
                SimulatingPhysicsChangedEvent(this);
            }
        }
        
        public override Type GetTargetSocketType()
        {
            return typeof(RoofSocket);
        }
    }
}