﻿using System;
using UnityEngine;

namespace ConstructionSystem
{
    [Serializable]
    public class MeshColliderData : IMeshColliderData
    {
        [SerializeField] private bool isTrigger = false;
        [SerializeField] private Mesh mesh = null;
        [SerializeField] private bool convex = true;
        [SerializeField] private bool inflateMesh = false;
        [SerializeField] private float skinWidth = 0.01f;
        [SerializeField] private Vector3 localPositionOffset = Vector3.zero;
        [SerializeField] private Vector3 localRotationOffset = Vector3.zero;
        [SerializeField] private Vector3 localScale = Vector3.one;
        [SerializeField] private Vector3 boundsAdjust = Vector3.zero;
        
        public bool IsTrigger => isTrigger;
        public Mesh Mesh => mesh;
        public bool Convex => convex;
        public bool InflateMesh => inflateMesh;
        public float SkinWidth => skinWidth;
        public Vector3 LocalPositionOffset => localPositionOffset;
        public Vector3 LocalRotationOffset => localRotationOffset;
        public Vector3 LocalScale => localScale;

        public Vector3 Center => boundsAdjust;
        public Vector3 Size => Vector3.Scale(mesh.bounds.size, localScale);

        public IConstructionCollider ApplyTo(IBuildingBlock block)
        {
            return ApplyTo((Component)block);
        }

        public IConstructionCollider ApplyTo(Component component)
        {
            return ApplyTo(component.gameObject);
        }

        public IConstructionCollider ApplyTo(GameObject gameObject)
        {
            var collider = ColliderCache.Get<MeshCollider>(gameObject.layer, localPositionOffset, Quaternion.Euler(localRotationOffset), localScale, isTrigger, gameObject.transform);
            collider.sharedMesh = mesh;
            collider.convex = convex;
            collider.inflateMesh = inflateMesh;
            collider.skinWidth = skinWidth;
            return new ConstructionCollider(collider);
        }

#if UNITY_EDITOR

        public void DrawGizmos(ITransform transform, Vector3 positionOffset, Vector3 rotationOffset, bool drawCenter)
        {
            var position = transform.Position;
            var rotation = transform.Rotation;
            var matrix = Gizmos.matrix;
            Gizmos.matrix = Matrix4x4.TRS(position + transform.TransformDirection(localPositionOffset + positionOffset), rotation * Quaternion.Inverse(Quaternion.Euler(localRotationOffset + rotationOffset)), Vector3.one);
            var color = Gizmos.color;

            if (drawCenter)
            {
                Gizmos.color = Color.magenta;
                Gizmos.DrawCube(Vector3.zero, new Vector3(0.1f, 0.1f, 0.1f));
            }

            Gizmos.color = color;
            Gizmos.matrix = Matrix4x4.TRS(transform.Position, transform.Rotation, Vector3.one);
            Gizmos.DrawMesh(mesh, localPositionOffset, Quaternion.Euler(localRotationOffset), localScale);
            Gizmos.matrix = matrix;
        }

#endif
    }
}