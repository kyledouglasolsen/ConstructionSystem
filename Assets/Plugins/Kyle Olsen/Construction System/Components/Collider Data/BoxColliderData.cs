﻿using System;
using UnityEngine;

namespace ConstructionSystem
{
    [Serializable]
    public class BoxColliderData : IBoxColliderData
    {
        [SerializeField] private bool isTrigger = false;
        [SerializeField] private Vector3 center = Vector3.zero;
        [SerializeField] private Vector3 size = Vector3.one;

        public bool IsTrigger => isTrigger;

        public Vector3 Center => center;
        public Vector3 Size => size;

        public IConstructionCollider ApplyTo(IBuildingBlock block)
        {
            return ApplyTo((Component)block);
        }

        public IConstructionCollider ApplyTo(Component component)
        {
            return ApplyTo(component.gameObject);
        }

        public IConstructionCollider ApplyTo(GameObject gameObject)
        {
            var collider = ColliderCache.Get<BoxCollider>(gameObject.layer, isTrigger, gameObject.transform);
            collider.center = center;
            collider.size = size;
            return new ConstructionCollider(collider);
        }

#if UNITY_EDITOR

        public void DrawGizmos(ITransform transform, Vector3 positionOffset, Vector3 rotationOffset, bool drawCenter)
        {
            var position = transform.Position;
            var rotation = transform.Rotation;
            var matrix = Gizmos.matrix;

            Gizmos.matrix = Matrix4x4.TRS(position + transform.TransformDirection(positionOffset), rotation * Quaternion.Euler(rotationOffset), Vector3.one);
            Gizmos.DrawCube(center, size + new Vector3(0.001f, 0.001f, 0.001f));
            Gizmos.matrix = matrix;
        }
        
#endif
    }
}