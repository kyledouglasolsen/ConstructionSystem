﻿using UnityEngine;

namespace ConstructionSystem
{
    public struct ConstructionCollider : IConstructionCollider
    {
        private Collider collider;
        private Collider[] colliders;

        public Collider this[int index] => collider ?? colliders?[index];
        public int Count { get; }

        public ConstructionCollider(Collider collider)
        {
            this.collider = collider;
            colliders = null;
            Count = 1;
        }

        public ConstructionCollider(Collider[] colliders)
        {
            this.colliders = colliders;
            collider = null;
            Count = colliders.Length;
        }

        public Collider Main()
        {
            return collider != null ? collider : colliders[0];
        }

        public void ReturnToCache()
        {
            if (collider != null)
            {
                ColliderCache.Return(collider);
            }

            if (colliders != null)
            {
                for (var i = 0; i < colliders.Length; ++i)
                {
                    ColliderCache.Return(colliders[i]);
                }
            }
        }
    }
}