﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ConstructionSystem
{
    [Serializable]
    public class MultiBoxColliderData : IBoxColliderData
    {
        [SerializeField] private BoxColliderData[] boxColliderDatas = new BoxColliderData[0];
        [SerializeField] private Vector3 boundsAdjust = Vector3.zero;

        public bool IsTrigger => false;

        public Vector3 Center => boundsAdjust;

        public Vector3 Size
        {
            get
            {
                Vector3 min = Vector3.zero, max = Vector3.zero;

                for (var i = 0; i < boxColliderDatas.Length; ++i)
                {
                    var extents = boxColliderDatas[i].Size * 0.5f;

                    if (i == 0)
                    {
                        min = boxColliderDatas[i].Center - extents;
                        max = boxColliderDatas[i].Center + extents;
                    }
                    else
                    {
                        min = Vector3.Min(min, boxColliderDatas[i].Center - extents);
                        max = Vector3.Max(max, boxColliderDatas[i].Center + extents);
                    }
                }

                return max - min;
            }
        }

        public IConstructionCollider ApplyTo(IBuildingBlock block)
        {
            return ApplyTo((Component)block);
        }

        public IConstructionCollider ApplyTo(Component component)
        {
            return ApplyTo(component.gameObject);
        }

        public IConstructionCollider ApplyTo(GameObject gameObject)
        {
            return new ConstructionCollider(InternalApplyTo(gameObject).ToArray());
        }

        private IEnumerable<Collider> InternalApplyTo(GameObject gameObject)
        {
            foreach (var box in boxColliderDatas)
            {
                yield return box.ApplyTo(gameObject).Main();
            }
        }

#if UNITY_EDITOR

        public void DrawGizmos(ITransform transform, Vector3 positionOffset, Vector3 rotationOffset, bool drawCenter)
        {
            foreach (var box in boxColliderDatas)
            {
                box.DrawGizmos(transform, positionOffset, rotationOffset, drawCenter);
            }
        }

#endif
    }
}