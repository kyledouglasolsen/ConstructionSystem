﻿namespace ConstructionSystem.BuildingSockets
{
    public class FloorSocket : BaseSocket
    {
        public override bool IsValidConnection(IBuildingBlock block)
        {
            return block is FloorBlock;
        }

        public override bool IsValidConnection(IBuildingSocket otherSocket)
        {
            return otherSocket is FloorSocket;
        }
    }
}