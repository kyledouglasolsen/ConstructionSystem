﻿namespace ConstructionSystem.BuildingSockets
{
    public class PillarSocket : BaseSocket
    {
        public override bool IsValidConnection(IBuildingBlock block)
        {
            return block is PillarBlock;
        }

        public override bool IsValidConnection(IBuildingSocket otherSocket)
        {
            return otherSocket is PillarSocket;
        }
    }
}