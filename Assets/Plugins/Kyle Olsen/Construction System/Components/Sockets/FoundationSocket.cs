﻿namespace ConstructionSystem.BuildingSockets
{
    public class FoundationSocket : BaseSocket
    {
        public override bool IsValidConnection(IBuildingBlock block)
        {
            return block is FoundationBlock;
        }

        public override bool IsValidConnection(IBuildingSocket otherSocket)
        {
            return otherSocket is FoundationSocket;
        }
    }
}