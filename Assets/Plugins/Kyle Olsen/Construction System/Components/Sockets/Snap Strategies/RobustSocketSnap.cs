﻿using UnityEngine;

namespace ConstructionSystem.BuildingSockets
{
    public class RobustSocketSnap : BaseSocketSnap
    {
        public static RobustSocketSnap Ins { get; } = new RobustSocketSnap();

        public override bool Snap(IBuildingBlock block, IBuildingSocket targetSocket, Vector3 buildPosition, Quaternion buildRotation)
        {
            IBuildingSocket connectingSocket = null;
            var startingPosition = block.Position;
            var startingRotation = block.Rotation;
            var closestDistance = Mathf.Infinity;
            var closestPosition = buildPosition;
            var closestRotation = buildRotation;

            foreach (var socket in block.ActiveSockets)
            {
                if (!socket.IsValidConnection(targetSocket))
                {
                    continue;
                }

                var worldOffset = Vector3.zero;
                var targetPosition = Vector3.zero;
                var targetRotation = Quaternion.identity;
                var validConditions = false;
                var startIndex = BlockPreviewer.CurrentRotationOffsetIndex;
                var offsetsLength = BlockPreviewer.RotationOffsets.Length;
                var endIndex = startIndex + offsetsLength;

                for (var i = startIndex; i < endIndex; ++i)
                {
                    var wrapIndex = (int)Mathf.Repeat(i, offsetsLength);
                    var evaluateRotationOffset = BlockPreviewer.RotationOffsets[wrapIndex];
                    var thisPosition = targetSocket.Position;
                    var connectingSocketPosition = targetSocket.OwningBlock.TransformPoint(socket.Data.LocalPositionOffset);
                    worldOffset = thisPosition - connectingSocketPosition;
                    targetPosition = targetSocket.OwningBlock.Position + worldOffset;
                    var activeSocketRotation = Quaternion.Inverse(Quaternion.Euler(socket.Data.LocalRotationOffset));
                    var thisOffsetDirection = Quaternion.Euler(targetSocket.Data.LocalRotationOffset) * targetSocket.OwningBlock.Forward;
                    targetRotation = Quaternion.LookRotation(evaluateRotationOffset * activeSocketRotation * thisOffsetDirection, targetSocket.OwningBlock.Up);
                    block.SetPositionAndRotation(targetPosition, targetRotation);
                    validConditions = block.EvaluateConditions();

                    if (validConditions)
                    {
                        break;
                    }
                }

                if (!validConditions)
                {
                    continue;
                }

                var distance = (targetSocket.Position + worldOffset - targetPosition).sqrMagnitude;

                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    closestPosition = targetPosition;
                    closestRotation = targetRotation;
                    connectingSocket = socket;
                }
            }

            if (connectingSocket != null)
            {
                block.SetPositionAndRotation(closestPosition, closestRotation);
                return true;
            }

            block.SetPositionAndRotation(startingPosition, startingRotation);
            return false;
        }
    }
}