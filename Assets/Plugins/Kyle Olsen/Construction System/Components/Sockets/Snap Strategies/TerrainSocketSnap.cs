﻿using UnityEngine;

namespace ConstructionSystem.BuildingSockets
{
    public class TerrainSocketSnap : BaseSocketSnap
    {
        public override bool Snap(IBuildingBlock block, IBuildingSocket targetSocket, Vector3 buildPosition, Quaternion buildRotation)
        {
            block.SetPositionAndRotation(buildPosition + block.Data.BuildPositionOffset, buildRotation * Quaternion.Inverse(Quaternion.Euler(block.Data.BuildRotationOffset)));
            return false;
        }
    }
}