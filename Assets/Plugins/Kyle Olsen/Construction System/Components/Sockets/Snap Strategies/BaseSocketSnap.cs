﻿using UnityEngine;

namespace ConstructionSystem.BuildingSockets
{
    public abstract class BaseSocketSnap : ISocketSnapStrategy
    {
        public abstract bool Snap(IBuildingBlock block, IBuildingSocket targetSocket, Vector3 buildPosition, Quaternion buildRotation);
    }
}