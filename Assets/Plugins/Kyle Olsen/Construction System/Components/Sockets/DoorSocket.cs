﻿namespace ConstructionSystem.BuildingSockets
{
    public class DoorSocket : BaseSocket
    {
        public override bool IsValidConnection(IBuildingBlock block)
        {
            return block is DoorBlock;
        }

        public override bool IsValidConnection(IBuildingSocket otherSocket)
        {
            return otherSocket is DoorSocket;
        }
    }
}