﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ConstructionSystem.BuildingSockets
{
    [CreateAssetMenu(fileName = "Socket Collection", menuName = ScriptableObjectPaths.Sockets + "Collection", order = 0)]
    public class SocketCollection : ScriptableObject, ISocketCollection
    {
        [SerializeField] private BaseSocketData[] sockets = new BaseSocketData[0];

        public IBuildingSocketData[] Sockets
        {
            get { return sockets; }
        }

        public bool Contains(IBuildingSocketData socketData)
        {
            foreach (var socket in Sockets)
            {
                if (socket == socketData)
                {
                    return true;
                }
            }

            return false;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator<IBuildingSocketData> GetEnumerator()
        {
            foreach (var socket in sockets)
            {
                if (socket != null)
                {
                    yield return socket;
                }
            }
        }
    }
}