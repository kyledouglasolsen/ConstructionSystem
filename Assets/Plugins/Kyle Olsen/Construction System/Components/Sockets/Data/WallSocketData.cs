﻿using System;
using UnityEngine;

namespace ConstructionSystem.BuildingSockets
{
    [CreateAssetMenu(fileName = InternalSocketName, menuName = ScriptableObjectPaths.Sockets + InternalSocketName, order = 1)]
    public class WallSocketData : BaseSocketData
    {
        private const string InternalSocketName = "Wall Socket";

        [SerializeField] private BoxColliderData boxColliderData = new BoxColliderData();

        public override IColliderData ColliderData => boxColliderData;
        protected override string SocketName => InternalSocketName;

        protected override Type GetSocketType()
        {
            return typeof(WallSocket);
        }
    }
}