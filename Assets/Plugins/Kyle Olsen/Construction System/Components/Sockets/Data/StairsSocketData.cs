﻿using System;
using UnityEngine;

namespace ConstructionSystem.BuildingSockets
{
    [CreateAssetMenu(fileName = InternalSocketName, menuName = ScriptableObjectPaths.Sockets + InternalSocketName, order = 1)]
    public class StairsSocketData : BaseSocketData
    {
        private const string InternalSocketName = "Stairs Socket";

        [SerializeField] private BoxColliderData boxColliderData = new BoxColliderData();

        protected override string SocketName
        {
            get { return InternalSocketName; }
        }

        protected override Type GetSocketType()
        {
            return typeof(StairsSocket);
        }

        public override IColliderData ColliderData
        {
            get { return boxColliderData; }
        }
    }
}