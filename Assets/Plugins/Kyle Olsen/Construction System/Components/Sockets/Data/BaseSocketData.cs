﻿using System;
using UnityEngine;

namespace ConstructionSystem.BuildingSockets
{
    public abstract class BaseSocketData : ScriptableObject, IBuildingSocketData
    {
#if UNITY_EDITOR
        [SerializeField] private bool drawGizmos = true;
        [SerializeField] private Color gizmoColor = new Color(0f, 0f, 0f, 0.2f);
#endif
        [SerializeField] private Vector3 localPositionOffset = Vector3.zero;
        [SerializeField] private Vector3 localRotationOffset = Vector3.zero;
        
        public abstract IColliderData ColliderData { get; }

        public Vector3 LocalPositionOffset => localPositionOffset;
        public Vector3 LocalRotationOffset => localRotationOffset;

        protected abstract string SocketName { get; }

        public virtual IBuildingSocket CreateSocket(IBuildingBlock block, bool isPreview)
        {
            var type = GetSocketType();
            var socket = new GameObject(SocketName, type).GetComponent<IBuildingSocket>();
            socket.Enabled = false;
            return InitializeSocket(block, socket, isPreview);
        }

        public virtual IBuildingSocket InitializeSocket(IBuildingBlock block, IBuildingSocket socket, bool isPreview)
        {
            socket.SetParent(block);
            socket.SetData(block, this, isPreview);
            return socket;
        }

        protected abstract Type GetSocketType();

#if UNITY_EDITOR
        public virtual void DrawGizmos(BaseBlock baseBlock)
        {
            if (!drawGizmos)
            {
                return;
            }

            var matrix = Gizmos.matrix;
            Gizmos.color = Color.magenta;
            Gizmos.matrix = Matrix4x4.TRS(baseBlock.Position + baseBlock.TransformDirection(LocalPositionOffset), baseBlock.Rotation * Quaternion.Euler(LocalRotationOffset), Vector3.one);
            Gizmos.DrawCube(Vector3.zero, new Vector3(0.1f, 0.1f, 0.1f));
            Gizmos.matrix = matrix;

            Gizmos.color = gizmoColor;
            ColliderData.DrawGizmos(baseBlock, LocalPositionOffset, LocalRotationOffset);

            Gizmos.color = Color.blue;
            Gizmos.DrawRay(baseBlock.Position + baseBlock.TransformDirection(localPositionOffset), Quaternion.Euler(localRotationOffset) * baseBlock.Forward);
        }
#endif
    }
}