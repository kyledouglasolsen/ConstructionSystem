﻿using System;
using UnityEngine;

namespace ConstructionSystem.BuildingSockets
{
    [CreateAssetMenu(fileName = InternalSocketName, menuName = ScriptableObjectPaths.Sockets + InternalSocketName, order = 1)]
    public class FoundationSocketData : BaseSocketData
    {
        private const string InternalSocketName = "Foundation Socket";

        [SerializeField] private BoxColliderData boxColliderData = new BoxColliderData();

        protected override string SocketName
        {
            get { return InternalSocketName; }
        }

        protected override Type GetSocketType()
        {
            return typeof(FoundationSocket);
        }

        public override IColliderData ColliderData
        {
            get { return boxColliderData; }
        }
    }
}