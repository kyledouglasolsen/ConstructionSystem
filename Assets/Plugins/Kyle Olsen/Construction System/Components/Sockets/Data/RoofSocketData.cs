﻿using System;
using UnityEngine;

namespace ConstructionSystem.BuildingSockets
{
    [CreateAssetMenu(fileName = InternalSocketName, menuName = ScriptableObjectPaths.Sockets + InternalSocketName, order = 1)]
    public class RoofSocketData : BaseSocketData
    {
        private const string InternalSocketName = "Roof Socket";

        [SerializeField] private BoxColliderData boxColliderData = new BoxColliderData();

        protected override string SocketName
        {
            get { return InternalSocketName; }
        }

        protected override Type GetSocketType()
        {
            return typeof(RoofSocket);
        }

        public override IColliderData ColliderData
        {
            get { return boxColliderData; }
        }

    }
}