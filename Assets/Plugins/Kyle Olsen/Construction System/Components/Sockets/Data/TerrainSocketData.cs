﻿using System;
using UnityEngine;

namespace ConstructionSystem.BuildingSockets
{
    [CreateAssetMenu(fileName = InternalSocketName, menuName = ScriptableObjectPaths.Sockets + InternalSocketName, order = 1)]
    public class TerrainSocketData : BaseSocketData
    {
        private const string InternalSocketName = "Terrain Socket";
        
        protected override string SocketName
        {
            get { return InternalSocketName; }
        }

        protected override Type GetSocketType()
        {
            return typeof(TerrainSocket);
        }

        public override IColliderData ColliderData
        {
            get { return null; }
        }

    }
}