﻿using System;
using UnityEngine;

namespace ConstructionSystem.BuildingSockets
{
    [CreateAssetMenu(fileName = InternalSocketName, menuName = ScriptableObjectPaths.Sockets + InternalSocketName, order = 1)]
    public class PillarSocketData : BaseSocketData
    {
        private const string InternalSocketName = "Pillar Socket";

        [SerializeField] private BoxColliderData boxColliderData = new BoxColliderData();

        protected override string SocketName
        {
            get { return InternalSocketName; }
        }

        protected override Type GetSocketType()
        {
            return typeof(PillarSocket);
        }

        public override IColliderData ColliderData
        {
            get { return boxColliderData; }
        }
    }
}