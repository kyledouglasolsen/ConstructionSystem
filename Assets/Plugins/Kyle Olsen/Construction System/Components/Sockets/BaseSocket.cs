﻿using UnityEngine;

namespace ConstructionSystem.BuildingSockets
{
    public abstract class BaseSocket : ConstructionBehaviour, IBuildingSocket
    {
        public bool Enabled
        {
            get { return enabled; }
            set { enabled = value; }
        }

        public uint SocketId { get; private set; }
        public IBuildingBlock OwningBlock { get; private set; }
        public IBuildingSocketData Data { get; private set; }
        public ISocketSnapStrategy SocketSnapStrategy { get; private set; }
        public IConstructionCollider ConstructionCollider { get; private set; }

        protected bool IsPreview { get; private set; }

        public void SetSocketId(uint socketId)
        {
            SocketId = socketId;
        }

        public void SetData(IBuildingBlock block, IBuildingSocketData data, bool isPreview)
        {
            IsPreview = isPreview;
            OwningBlock = block;
            Data = data;
            SocketSnapStrategy = CreateNewSnapStrategy();
            ApplySocketData(Data, isPreview);
        }

        public bool SnapBlockTo(IBuildingBlock block, Vector3 buildPosition, Quaternion buildRotation)
        {
            return SocketSnapStrategy.Snap(block, this, buildPosition, buildRotation);
        }

        public abstract bool IsValidConnection(IBuildingBlock block);
        public abstract bool IsValidConnection(IBuildingSocket otherSocket);

        protected virtual ISocketSnapStrategy CreateNewSnapStrategy()
        {
            return RobustSocketSnap.Ins;
        }

        protected virtual void ApplySocketData(IBuildingSocketData applyingData, bool isPreview)
        {
            gameObject.layer = ConstructionLayers.BuildingSocketLayer;
            transform.localPosition = applyingData.LocalPositionOffset;
            transform.localEulerAngles = applyingData.LocalRotationOffset;

            if (!isPreview && enabled && ConstructionCollider == null)
            {
                ConstructionCollider = applyingData.ColliderData.ApplyTo(gameObject);
            }
        }

        public void Delete()
        {
            ConstructionCollider?.ReturnToCache();
            ConstructionCollider = null;
        }

        private void OnEnable()
        {
            if (!IsPreview && Data?.ColliderData != null && ConstructionCollider == null)
            {
                ConstructionCollider = Data.ColliderData.ApplyTo(gameObject);
            }
        }
        
        private void OnDisable()
        {
            Delete();
        }
    }
}