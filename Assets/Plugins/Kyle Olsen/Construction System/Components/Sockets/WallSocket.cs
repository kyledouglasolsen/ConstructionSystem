﻿namespace ConstructionSystem.BuildingSockets
{
    public class WallSocket : BaseSocket
    {
        public override bool IsValidConnection(IBuildingBlock block)
        {
            return block is WallBlock;
        }

        public override bool IsValidConnection(IBuildingSocket otherSocket)
        {
            return otherSocket is WallSocket;
        }
    }
}