﻿namespace ConstructionSystem.BuildingSockets
{
    public class DoorFrameSocket : WallSocket
    {
        public override bool IsValidConnection(IBuildingBlock block)
        {
            return block is DoorFrameBlock;
        }

        public override bool IsValidConnection(IBuildingSocket otherSocket)
        {
            return otherSocket is DoorFrameSocket;
        }
    }
}