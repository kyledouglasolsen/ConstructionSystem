﻿namespace ConstructionSystem.BuildingSockets
{
    public class StairsSocket : BaseSocket
    {
        public override bool IsValidConnection(IBuildingBlock block)
        {
            return block is StairsBlock;
        }

        public override bool IsValidConnection(IBuildingSocket otherSocket)
        {
            return otherSocket is StairsSocket;
        }
    }
}