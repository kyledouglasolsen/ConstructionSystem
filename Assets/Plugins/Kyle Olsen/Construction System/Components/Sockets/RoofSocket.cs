﻿namespace ConstructionSystem.BuildingSockets
{
    public class RoofSocket : BaseSocket
    {
        public override bool IsValidConnection(IBuildingBlock block)
        {
            return block is RoofBlock;
        }

        public override bool IsValidConnection(IBuildingSocket otherSocket)
        {
            return otherSocket is RoofSocket;
        }
    }
}