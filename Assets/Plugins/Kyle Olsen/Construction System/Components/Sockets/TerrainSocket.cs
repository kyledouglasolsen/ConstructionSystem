﻿using UnityEngine;

namespace ConstructionSystem.BuildingSockets
{
    public class TerrainSocket : BaseSocket
    {
        [SerializeField] private TerrainSocketData data = null;

        protected override ISocketSnapStrategy CreateNewSnapStrategy()
        {
            return new TerrainSocketSnap();
        }

        public override bool IsValidConnection(IBuildingBlock block)
        {
            return true;
        }

        public override bool IsValidConnection(IBuildingSocket otherSocket)
        {
            return true;
        }

        protected override void ApplySocketData(IBuildingSocketData applyingData, bool isPreview)
        {
        }

        private void Awake()
        {
            SetData(null, data, false);
        }
    }
}