﻿namespace ConstructionSystem.BuildingSockets
{
    public class WindowSocket : BaseSocket
    {
        public override bool IsValidConnection(IBuildingBlock block)
        {
            return block is WindowBlock;
        }

        public override bool IsValidConnection(IBuildingSocket otherSocket)
        {
            return otherSocket is WindowSocket;
        }
    }
}