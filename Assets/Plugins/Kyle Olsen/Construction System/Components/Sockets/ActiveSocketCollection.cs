﻿using System.Collections;
using System.Collections.Generic;

namespace ConstructionSystem.BuildingSockets
{
    public class ActiveSocketCollection : IActiveSocketCollection
    {
        private uint nextInternalId;
        private IBuildingBlock block;
        private ISocketCollection socketCollection;
        private bool isPreview;
        private List<IBuildingSocket> sockets;

        private List<IBuildingSocket> Sockets
        {
            get
            {
                if (sockets == null && socketCollection != null)
                {
                    sockets = new List<IBuildingSocket>(socketCollection.Sockets.Length);

                    foreach (var socket in socketCollection)
                    {
                        Add(socket.CreateSocket(block, isPreview));
                    }
                }

                return sockets;
            }
        }

        public ActiveSocketCollection(IBuildingBlock block, ISocketCollection socketCollection, bool isPreview)
        {
            this.block = block;
            this.isPreview = isPreview;
            this.socketCollection = socketCollection;
        }

        public void Add(IBuildingSocket socket)
        {
            Sockets.Add(socket);
            socket.SetSocketId(nextInternalId++);
        }

        public void Remove(IBuildingSocket socket)
        {
            Sockets.Remove(socket);
        }

        public bool Contains(IBuildingSocket socket)
        {
            return Sockets.Contains(socket);
        }

        public bool CanConnectTo(IBuildingSocket socket, out IBuildingSocket connectingSocket)
        {
            foreach (var active in Sockets)
            {
                if (active.IsValidConnection(socket) || socket.IsValidConnection(active))
                {
                    connectingSocket = socket;
                    return true;
                }
            }

            connectingSocket = null;
            return false;
        }

        public IBuildingSocket GetSocketById(uint id)
        {
            foreach (var socket in Sockets)
            {
                if (socket?.SocketId == id)
                {
                    return socket;
                }
            }

            return null;
        }

        public void DeleteAll()
        {
            if (sockets != null)
            {
                foreach (var socket in sockets)
                {
                    socket?.Delete();
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator<IBuildingSocket> GetEnumerator()
        {
            foreach (var socket in Sockets)
            {
                yield return socket;
            }
        }
    }
}