﻿using UnityEngine;

namespace ConstructionSystem.Conditions
{
    public abstract class OverlapCollisionCheck : BaseCondition
    {
#if UNITY_EDITOR
        [SerializeField] private bool drawGizmos = true;
        [SerializeField] private bool debugHits = false;
#endif
        [SerializeField] private bool ignoreSelf = true;
        [SerializeField] private bool mustContainAll = true;
        [SerializeField] private BaseBlockData[] mustContainData = new BaseBlockData[0];
        [SerializeField] private BlockDataLookup ignoredData = new BlockDataLookup();
        private bool[] foundData;

        public abstract SerializedOverlap[] Overlaps { get; }

        public override bool Evaluate(IBuildingBlock block)
        {
            InitializeAndClear();

            foreach (var overlap in Overlaps)
            {
                var result = overlap.From(block);

                if (IsValidOverlap(block, ref result))
                {
                    continue;
                }

                return false;
            }

            return FoundAllRequiredData();
        }

        private bool IsValidOverlap(IBuildingBlock block, ref OverlapResult result)
        {
            var blockBatch = block as IBatchCollision;

            for (var i = 0; i < result.Count; ++i)
            {
                var collider = result[i];
                var hitBlock = collider.GetComponentInParent<IBuildingBlock>();
                var hitBatch = hitBlock as IBatchCollision;

                if (ignoreSelf)
                {
                    if (blockBatch != null && hitBatch != null)
                    {
                        var closestDistance = float.MaxValue;
                        IBuildingBlock closestBlock = null;

                        for (var j = 0; j < hitBatch.BlockBatch.Blocks.Count; ++j)
                        {
                            var batchedBlock = hitBatch.BlockBatch.Blocks[j];

                            if (batchedBlock == null)
                            {
                                continue;
                            }

                            if (batchedBlock.Bounds.Contains(result.Position))
                            {
                                closestBlock = batchedBlock;
                                break;
                            }

                            var sqrDistance = (batchedBlock.Bounds.ClosestPoint(result.Position) - result.Position).sqrMagnitude;

                            if (sqrDistance < closestDistance)
                            {
                                closestDistance = sqrDistance;
                                closestBlock = batchedBlock;
                            }
                        }

                        if (closestBlock != null)
                        {
                            hitBlock = closestBlock;
                        }
                    }

                    if (hitBlock == block)
                    {
                        continue;
                    }
                }

                if (hitBlock != null && (IgnoreBlock(hitBlock) || IsMustContainBlock(hitBlock)))
                {
                    continue;
                }

#if UNITY_EDITOR
                if (debugHits)
                {
                    Debug.Log("Hit: " + collider.gameObject.name + " is preventing block placement", collider.gameObject);
                }
#endif
                return false;
            }

            return true;
        }

        private bool IgnoreBlock(IBuildingBlock block)
        {
            if (!ignoredData.Contains(block.Data))
            {
                return false;
            }

#if UNITY_EDITOR
            if (debugHits)
            {
                var blockCo = (block as Component);
                Debug.Log("Ignoring Hit: " + block, blockCo != null ? blockCo.gameObject : null);
            }
#endif

            return true;
        }

        private bool IsMustContainBlock(IBuildingBlock block)
        {
            var foundMustContain = false;

            for (var i = 0; i < mustContainData.Length; ++i)
            {
                if (!Equals(block.Data, mustContainData[i]))
                {
                    continue;
                }

#if UNITY_EDITOR
                if (debugHits)
                {
                    var blockCo = (block as Component);
                    Debug.Log("Found Must Hit: " + block, blockCo != null ? blockCo.gameObject : null);
                }
#endif

                foundMustContain = true;
                SetFound(i);
                break;
            }

            return foundMustContain;
        }

        private void SetFound(int index)
        {
            foundData[index] = true;
        }

        private bool FoundAllRequiredData()
        {
            var foundOne = !(foundData.Length > 0);

            for (var i = 0; i < foundData.Length; ++i)
            {
                if (foundData[i])
                {
                    foundOne = true;
                    continue;
                }

                if (mustContainAll)
                {
#if UNITY_EDITOR
                    if (debugHits)
                    {
                        Debug.LogError("Didn't find must hit: " + mustContainData[i]);
                    }
#endif

                    return false;
                }
            }

            return mustContainAll || foundOne;
        }

        private void ClearFoundData()
        {
            for (var i = 0; i < foundData.Length; ++i)
            {
                foundData[i] = false;
            }
        }

        private void InitializeAndClear()
        {
            if (foundData == null || foundData.Length != mustContainData.Length)
            {
                foundData = new bool[mustContainData.Length];
            }
            else
            {
                ClearFoundData();
            }
        }

#if UNITY_EDITOR

        [ContextMenu("On Validate")]
        public override void OnValidate()
        {
            ignoredData.OnValidate();
        }

        public sealed override void DrawGizmos(IBuildingBlock block)
        {
            if (!drawGizmos)
            {
                return;
            }

            InitializeAndClear();
            var isValid = Evaluate(block);

            for (var i = 0; i < Overlaps.Length; ++i)
            {
                Gizmos.color = isValid ? Color.green : Color.red;
                Overlaps[i].DrawGizmo(block);
            }
        }

#endif
    }
}