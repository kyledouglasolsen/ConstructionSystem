﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ConstructionSystem.Conditions
{
    [CreateAssetMenu(fileName = "Condition Collection", menuName = ScriptableObjectPaths.Conditions + "Collection", order = 0)]
    public class ConditionCollection : ScriptableObject, IConditionCollection
    {
        [SerializeField] private ConditionEntry[] conditions = new ConditionEntry[0];

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator<IConstructionCondition> GetEnumerator()
        {
            foreach (var entry in conditions)
            {
                if (entry.Active && entry.Condition != null)
                {
                    yield return entry.Condition;
                }
            }
        }
        
        public bool Evaluate(IBuildingBlock block)
        {
            IConstructionCondition failedCondition;
            return Evaluate(block, out failedCondition);
        }

        public virtual bool Evaluate(IBuildingBlock block, out IConstructionCondition failedCondition)
        {
            foreach (var condition in conditions)
            {
                if (!condition.Active || condition.Condition.Evaluate(block))
                {
                    continue;
                }

                failedCondition = condition.Condition;
                return false;
            }

            failedCondition = null;
            return true;
        }
        
        [Serializable]
        private class ConditionEntry
        {
            [SerializeField] private bool active = true;
            [SerializeField] private BaseCondition condition = null;
            
            public bool Active => active;
            public BaseCondition Condition => condition;
        }
    }
}