﻿using System;
using UnityEngine;

namespace ConstructionSystem.Conditions
{
    [Serializable]
    public class SerializedRaycast : SerializedPhysicsCast
    {
        public override bool From(IBuildingBlock block, out RaycastHit hit)
        {
            var ray = CreateRayFromBlock(block);
            var hitCount = Physics.RaycastNonAlloc(ray, CachedHitBuffer, Distance, Layermask);
            var offset = ray.direction * 0.1f;

            for (var i = 0; i < hitCount; ++i)
            {
                var cachedHit = CachedHitBuffer[i];
                var hitPoint = cachedHit.point;

                if (hitPoint == Vector3.zero)
                {
                    hitPoint = ray.origin;
                }
                
                var foundBlock = cachedHit.collider.GetComponentInParent<IBuildingBlock>();
                var batch = foundBlock as IBatchCollision;

                if (batch != null && batch.IsBatched)
                {
                    var blockBatch = batch.BlockBatch;
                    foundBlock = blockBatch.GetBlockFromPoint(hitPoint + offset);
                }

                if (foundBlock == block || (foundBlock != null && IgnoredData.Contains(foundBlock.Data)))
                {
                    continue;
                }

                hit = cachedHit;
                return true;
            }

            hit = new RaycastHit();
            return false;
        }

#if UNITY_EDITOR

        protected override void OnDrawGizmo(Vector3 position, Vector3 direction)
        {
            Gizmos.DrawRay(position, direction);
        }

#endif
    }
}