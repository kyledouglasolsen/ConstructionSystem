﻿using UnityEngine;

namespace ConstructionSystem.Conditions
{
    public abstract class BaseCondition : ScriptableObject, IConstructionCondition
    {
        public abstract bool Evaluate(IBuildingBlock block);

#if UNITY_EDITOR
        public virtual void OnValidate()
        {
        }
        
        public abstract void DrawGizmos(IBuildingBlock block);
#endif
    }
}
