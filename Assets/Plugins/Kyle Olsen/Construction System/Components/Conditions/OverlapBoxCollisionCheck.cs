﻿using UnityEngine;

namespace ConstructionSystem.Conditions
{
    [CreateAssetMenu(fileName = "Overlap Box Check", menuName = ScriptableObjectPaths.Conditions + "Overlap Box Check", order = 1000)]
    public class OverlapBoxCollisionCheck : OverlapCollisionCheck
    {
        [SerializeField] private SerializedOverlapBox[] overlapBoxes = new SerializedOverlapBox[0];

        public override SerializedOverlap[] Overlaps => overlapBoxes;
    }
}