﻿using System;
using UnityEngine;

namespace ConstructionSystem.Conditions
{
    [Serializable]
    public abstract class SerializedOverlap
    {
        protected static Collider[] CachedColliderBuffer = new Collider[100];

        [SerializeField] protected LayerMask Layermask = Physics.DefaultRaycastLayers;
        [SerializeField] protected Vector3 LocalOffset = Vector3.zero;

        public OverlapResult From(IBuildingBlock block)
        {
            var position = block.Position + block.TransformDirection(LocalOffset);
            var rotation = block.Rotation;
            return new OverlapResult(position, rotation, CachedColliderBuffer, InternalOverlap(position, rotation));
        }

        protected abstract int InternalOverlap(Vector3 position, Quaternion rotation);

#if UNITY_EDITOR

        public virtual void OnValidate()
        {
        }

        public void DrawGizmo(IBuildingBlock block)
        {
            var position = block.Position + block.TransformDirection(LocalOffset);
            var rotation = block.Rotation;
            OnDrawGizmos(position, rotation);
        }

        protected abstract void OnDrawGizmos(Vector3 position, Quaternion rotation);
#endif
    }
}