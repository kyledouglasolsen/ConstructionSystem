﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ConstructionSystem.Conditions
{
    public struct OverlapResult
    {
        private Collider[] colliders;

        public OverlapResult(Vector3 position, Quaternion rotation, Collider[] colliders, int count)
        {
            this.colliders = colliders;
            Position = position;
            Rotation = rotation;
            Count = count;
        }

        public Collider this[int index] => colliders[index];
        public Vector3 Position { get; }
        public Quaternion Rotation { get; }
        public int Count { get; }
    }
}