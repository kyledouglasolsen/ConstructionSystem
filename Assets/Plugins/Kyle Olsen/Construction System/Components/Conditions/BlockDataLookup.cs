﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ConstructionSystem.Conditions
{
    [Serializable]
    public class BlockDataLookup : IEnumerable<BaseBlockData>
    {
        [SerializeField] private BaseBlockData[] blockData = new BaseBlockData[0];
        private HashSet<IBuildingBlockData> dataLookup;

        public int Length => blockData.Length;
        public IBuildingBlockData this[int index] => blockData[index];
        protected HashSet<IBuildingBlockData> DataLookup => dataLookup ?? CreateLookup();

        public bool Contains(IBuildingBlockData data)
        {
            return DataLookup.Contains(data);
        }

        private HashSet<IBuildingBlockData> CreateLookup()
        {
            if (dataLookup == null)
            {
                dataLookup = new HashSet<IBuildingBlockData>(blockData);
            }
            else
            {
                dataLookup.Clear();

                foreach (var data in blockData)
                {
                    dataLookup.Add(data);
                }
            }

            return dataLookup;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator<BaseBlockData> GetEnumerator()
        {
            foreach (var data in blockData)
            {
                yield return data;
            }
        }

#if UNITY_EDITOR

        public void OnValidate()
        {
            CreateLookup();
        }

#endif
    }
}