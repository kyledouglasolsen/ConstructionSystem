﻿using UnityEngine;

namespace ConstructionSystem.Conditions
{
    [CreateAssetMenu(fileName = "Grounded Raycast", menuName = ScriptableObjectPaths.Conditions + "Grounded Raycast", order = 1000)]
    public class GroundedRaycast : Grounded
    {
        [SerializeField] private SerializedRaycast[] raycasts = new SerializedRaycast[0];

        public override SerializedPhysicsCast[] PhysicsCasts => raycasts;
    }
}