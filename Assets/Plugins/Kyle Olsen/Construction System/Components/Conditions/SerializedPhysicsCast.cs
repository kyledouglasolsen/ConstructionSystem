﻿using System;
using UnityEngine;

namespace ConstructionSystem.Conditions
{
    [Serializable]
    public abstract class SerializedPhysicsCast
    {
        protected static RaycastHit[] CachedHitBuffer = new RaycastHit[100];

        [SerializeField] protected LayerMask Layermask = Physics.DefaultRaycastLayers;
        [SerializeField] protected Vector3 LocalOffset = Vector3.zero;
        [SerializeField] protected Vector3 Direction = Vector3.down;
        [SerializeField] protected float Distance = 1f;
        [SerializeField] protected BlockDataLookup IgnoredData = new BlockDataLookup();

        public abstract bool From(IBuildingBlock block, out RaycastHit hit);

        protected Ray CreateRayFromBlock(IBuildingBlock block)
        {
            var batch = block as IBatchCollision;

            if (batch != null && batch.IsBatched)
            {
                var height = block.Data.ColliderData.Size.y;
                var distanceFromBottom = Mathf.Abs(block.Bounds.max.y - batch.BlockBatch.Bounds.min.y);
                var thisStackHeight = Mathf.RoundToInt(distanceFromBottom / height) - 1;

                if (thisStackHeight < 0)
                {
                    thisStackHeight = 0;
                }

                var castPosition = block.TransformPoint(LocalOffset - Vector3.up * thisStackHeight * height);
                return new Ray(castPosition, block.TransformDirection(Direction));
            }

            return new Ray(block.TransformPoint(LocalOffset), block.TransformDirection(Direction));
        }

#if UNITY_EDITOR

        public virtual void OnValidate()
        {
            if (Direction == Vector3.zero)
            {
                Debug.LogError("Direction must be non-zero!");
                Direction = Vector3.down;
            }
        }

        public virtual void DrawGizmo(IBuildingBlock block, bool debugHits)
        {
            RaycastHit hit;
            var ray = CreateRayFromBlock(block);
            var didHit = From(block, out hit);

            if (didHit && debugHits)
            {
                Debug.Log($"Hit {hit.collider.name}", hit.collider.gameObject);
                Gizmos.color = Color.red;
                Gizmos.DrawLine(Vector3.zero, hit.point);
                Gizmos.DrawLine(hit.point - Vector3.right, hit.point + Vector3.right);
                Gizmos.DrawLine(hit.point + Vector3.forward, hit.point - Vector3.forward);
                Gizmos.DrawSphere(hit.point, 0.5f);
            }

            Gizmos.color = didHit ? Color.green : Color.red;
            var drawDirection = ray.direction * (didHit ? hit.distance : Distance);
            OnDrawGizmo(ray.origin, drawDirection);
        }

        protected abstract void OnDrawGizmo(Vector3 position, Vector3 direction);
#endif
    }
}