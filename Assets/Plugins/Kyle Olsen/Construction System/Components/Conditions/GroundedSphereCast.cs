﻿using UnityEngine;

namespace ConstructionSystem.Conditions
{
    [CreateAssetMenu(fileName = "Grounded SphereCast", menuName = ScriptableObjectPaths.Conditions + "Grounded SphereCast", order = 1000)]
    public class GroundedSphereCast : Grounded
    {
        [SerializeField] private SerializedSphereCast[] sphereCasts = new SerializedSphereCast[0];

        public override SerializedPhysicsCast[] PhysicsCasts => sphereCasts;
    }
}