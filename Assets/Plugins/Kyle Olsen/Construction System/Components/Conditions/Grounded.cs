﻿using UnityEngine;

namespace ConstructionSystem.Conditions
{
    public abstract class Grounded : BaseCondition
    {
#if UNITY_EDITOR
        [SerializeField] private bool debugHits = false;
        [SerializeField] private bool drawGizmos = true;
#endif

        public abstract SerializedPhysicsCast[] PhysicsCasts { get; }

        public override bool Evaluate(IBuildingBlock block)
        {
            foreach (var cast in PhysicsCasts)
            {
                RaycastHit hit;
                var grounded = cast.From(block, out hit);

                if (!grounded)
                {
                    return false;
                }
            }

            return true;
        }

#if UNITY_EDITOR

        public override void OnValidate()
        {
            foreach (var raycast in PhysicsCasts)
            {
                raycast.OnValidate();
            }
        }

        public override void DrawGizmos(IBuildingBlock block)
        {
            if (!drawGizmos)
            {
                return;
            }

            foreach (var raycast in PhysicsCasts)
            {
                raycast.DrawGizmo(block, debugHits);
            }
        }

#endif
    }
}