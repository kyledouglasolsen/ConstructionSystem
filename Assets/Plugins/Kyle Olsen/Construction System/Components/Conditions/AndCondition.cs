﻿using UnityEngine;

namespace ConstructionSystem.Conditions
{
    [CreateAssetMenu(fileName = "And Condition", menuName = ScriptableObjectPaths.Conditions + "And", order = 1000)]
    public class AndCondition : BaseCondition
    {
        [SerializeField] private BaseCondition[] conditions = new BaseCondition[0];

        public override bool Evaluate(IBuildingBlock block)
        {
            foreach (var condition in conditions)
            {
                if (!condition.Evaluate(block))
                {
                    return false;
                }
            }

            return conditions.Length > 0;
        }

#if UNITY_EDITOR
        public override void OnValidate()
        {
            foreach (var condition in conditions)
            {
                condition.OnValidate();
            }
        }

        public override void DrawGizmos(IBuildingBlock block)
        {
            foreach (var condition in conditions)
            {
                condition.DrawGizmos(block);
            }
        }
#endif
    }
}
