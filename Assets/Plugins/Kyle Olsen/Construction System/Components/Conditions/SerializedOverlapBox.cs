﻿using System;
using UnityEngine;

namespace ConstructionSystem.Conditions
{
    [Serializable]
    public class SerializedOverlapBox : SerializedOverlap
    {
        [SerializeField] private Vector3 size = Vector3.one;

        protected override int InternalOverlap(Vector3 position, Quaternion rotation)
        {
            return Physics.OverlapBoxNonAlloc(position, size / 2f, CachedColliderBuffer, rotation, Layermask);
        }

#if UNITY_EDITOR

        protected override void OnDrawGizmos(Vector3 position, Quaternion rotation)
        {
            var matrix = Gizmos.matrix;
            Gizmos.matrix = Matrix4x4.TRS(position, rotation, Vector3.one);
            Gizmos.DrawCube(Vector3.zero, size);
            Gizmos.matrix = matrix;
        }

#endif
    }
}