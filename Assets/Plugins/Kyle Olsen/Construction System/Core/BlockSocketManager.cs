﻿using System;
using System.Collections.Generic;

namespace ConstructionSystem
{
    public static class BlockSocketManager
    {
        private static Dictionary<Type, bool> activeTypeLookup = new Dictionary<Type, bool>();
        
        public static void EnableValidSockets(IBuildingBlock block)
        {
            var socketType = block.GetTargetSocketType();
            EnableSocketType(socketType);
        }
        
        public static void DisableValidSockets(IBuildingBlock block)
        {
            var socketType = block.GetTargetSocketType();
            DisableSocketType(socketType);
        }

        public static void EnableSocketType(Type type)
        {
            SetSocketTypeEnabled(type, true);
        }

        public static void DisableSocketType(Type type)
        {
            SetSocketTypeEnabled(type, false);
        }

        public static void SetSocketTypeEnabled(Type type, bool enabled)
        {
            activeTypeLookup[type] = enabled;
        }

        public static void DisableAllSocketTypes()
        {
            foreach (var type in activeTypeLookup.Keys)
            {
                SetSocketTypeEnabled(type, false);
            }
        }

        public static bool GetSocketTypeActive(Type type)
        {
            bool active;
            
            if(activeTypeLookup.TryGetValue(type, out active))
            {
                return active;
            }
        
            return false;
        }
    }
}