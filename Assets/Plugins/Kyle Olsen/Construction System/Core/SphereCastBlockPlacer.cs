﻿using UnityEngine;

namespace ConstructionSystem
{
    public class SphereCastBlockPlacer : CastBlockPlacer
    {
        private float radius;

        public SphereCastBlockPlacer(Transform castTransform, float radius, float distance, LayerMask layermask) : base(castTransform, distance, layermask)
        {
            this.radius = radius;
        }

        protected override int DoCast(Ray ray, float distance, LayerMask layermask)
        {
            return Physics.SphereCastNonAlloc(ray, radius, RaycastHitCache, distance, layermask, QueryTriggerInteraction.Collide);
        }

        protected override void OnSetPreviewBlock(IBuildingBlock previewBlock)
        {
        }
    }
}