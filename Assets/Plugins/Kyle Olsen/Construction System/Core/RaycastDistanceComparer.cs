﻿using System.Collections.Generic;
using UnityEngine;

namespace ConstructionSystem
{
    public class RaycastDistanceComparer : IComparer<RaycastHit>
    {
        public static RaycastDistanceComparer Instance = new RaycastDistanceComparer();

        private RaycastDistanceComparer()
        {
        }

        public int Compare(RaycastHit a, RaycastHit b)
        {
            if (Mathf.Approximately(a.distance, b.distance))
            {
                return 0;
            }

            if (a.distance < b.distance)
            {
                return -1;
            }

            return 1;
        }
    }
}