﻿//using UnityEngine;
//
//namespace ConstructionSystem
//{
//    public struct StaticBounds
//    {
//        private readonly int hashCode;
//
//        public Vector3 Center { get; }
//        public Vector3 Size { get; }
//        public Vector3 Extents { get; }
//        public Vector3 Min { get; }
//        public Vector3 Max { get; }
//
//        public StaticBounds(Vector3 center, Vector3 size)
//        {
//            Center = center;
//            Size = size;
//            Extents = size * 0.5f;
//            Min = center - Extents;
//            Max = center + Extents;
//            hashCode = center.GetHashCode() ^ Extents.GetHashCode() << 2;
//        }
//
//        public StaticBounds(Bounds bounds) : this(bounds.center, bounds.size)
//        {
//        }
//
//        public static bool operator ==(StaticBounds lhs, StaticBounds rhs)
//        {
//            return lhs.Center == rhs.Center && lhs.Extents == rhs.Extents;
//        }
//
//        public static bool operator !=(StaticBounds lhs, StaticBounds rhs)
//        {
//            return !(lhs == rhs);
//        }
//
//        public bool Contains(Vector3 point)
//        {
//            return (point.x > Min.x) && (point.y > Min.y) && (point.z > Min.z) && (point.x < Max.x) && (point.y < Max.y) && (point.z < Max.z);
//        }
//
//        public bool Intersects(StaticBounds bounds)
//        {
//            return (Min.x <= bounds.Max.x) && (Max.x >= bounds.Min.x) && (Min.y <= Max.y) && (Max.y >= Min.y) && (Min.z <= Max.z) && (Max.z >= Min.z);
//        }
//
//        public override int GetHashCode()
//        {
//            return hashCode;
//        }
//
//        public override bool Equals(object other)
//        {
//            if (!(other is StaticBounds)) return false;
//            var bounds = (StaticBounds)other;
//            return Center.Equals(bounds.Center) && Extents.Equals(bounds.Extents);
//        }
//
//        public override string ToString()
//        {
//            return $"Center: {Center}, Extents: {Extents}";
//        }
//
//        public string ToString(string format)
//        {
//            return $"Center: {Center.ToString(format)}, Extents: {Extents.ToString(format)}";
//        }
//    }
//}