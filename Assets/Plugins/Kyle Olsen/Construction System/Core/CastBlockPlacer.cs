﻿using UnityEngine;

namespace ConstructionSystem
{
    public abstract class CastBlockPlacer : BlockPlacer
    {
        protected static RaycastHit[] RaycastHitCache = new RaycastHit[100];
        protected static int CacheCount;

        private Transform castTransform;
        private float distance;
        private LayerMask layermask;

        protected CastBlockPlacer(Transform castTransform, float distance, LayerMask layermask)
        {
            this.castTransform = castTransform;
            this.distance = distance;
            this.layermask = layermask;
        }

        protected override bool OnCast()
        {
            var ray = new Ray(castTransform.position, castTransform.forward);
            CacheCount = DoCast(ray, distance, layermask);

            var foundSocket = SocketFinder.FindValid(PreviewBlock, ray, RaycastHitCache, CacheCount, SnappingSocketData);
            SetSnappingData(foundSocket);

            if (foundSocket.HitSocket != null)
            {
                SetPosition(PreviewBlock.Position);
                SetRotation(PreviewBlock.Rotation);

                return true;
            }

            SetPosition(ray.origin + ray.direction * distance);

            if (PreviewBlock != null)
            {
                PreviewBlock.SetPositionAndRotation(BuildPosition, BuildRotation);
            }

            return false;
        }

        protected abstract int DoCast(Ray ray, float distance, LayerMask layermask);
    }
}