﻿namespace ConstructionSystem
{
    public static class ScriptableObjectPaths
    {
        public const string BlockData = "Construction System/Block Data/";
        public const string Conditions = "Construction System/Conditions/";
        public const string Sockets = "Construction System/Sockets/";
    }
}
