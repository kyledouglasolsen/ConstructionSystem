﻿using UnityEngine;

namespace ConstructionSystem
{
    public class SocketProximity : MonoBehaviour
    {
        private static readonly Collider[] OverlapCache = new Collider[1000];
        private static readonly PooledQueue<FastList<IBuildingBlock>> VisibleBlocksPool = new PooledQueue<FastList<IBuildingBlock>>(() => new FastList<IBuildingBlock>(100));

        [SerializeField] private float radius = 10f;
        private float sqrRadius;

        private FastList<IBuildingBlock> visibleBlocks = new FastList<IBuildingBlock>(100);

        private void Update()
        {
            var here = transform.position;
            var newVisible = VisibleBlocksPool.Dequeue();
            newVisible.Clear();

            var count = Physics.OverlapSphereNonAlloc(transform.position, radius, OverlapCache, 1 << ConstructionLayers.BuildingLayer, QueryTriggerInteraction.Collide);

            for (var i = 0; i < count; ++i)
            {
                var block = OverlapCache[i].GetComponentInParent<IBuildingBlock>();

                if (UnityHelpers.IsNull(block))
                {
                    continue;
                }

                var batch = block as IBatchCollision;

                if (batch == null)
                {
                    newVisible.Add(block);

                    if (!visibleBlocks.Contains(block))
                    {
                        OnAddVisible(block);
                        visibleBlocks.Remove(block);
                    }
                }
                else
                {
                    foreach (var batchedBlock in batch.BlockBatch.Blocks)
                    {
                        if ((here - batchedBlock.Position).sqrMagnitude > sqrRadius)
                        {
                            continue;
                        }

                        newVisible.Add(batchedBlock);

                        if (!visibleBlocks.Contains(batchedBlock))
                        {
                            OnAddVisible(batchedBlock);
                            visibleBlocks.Remove(batchedBlock);
                        }
                    }
                }
            }

            for (var i = 0; i < visibleBlocks.Count; ++i)
            {
                if (UnityHelpers.IsNull(visibleBlocks[i]))
                {
                    continue;
                }

                if (!newVisible.Contains(visibleBlocks[i]))
                {
                    OnRemoveVisible(visibleBlocks[i]);
                }
            }

            VisibleBlocksPool.Enqueue(visibleBlocks);
            visibleBlocks = newVisible;
        }

        private void OnAddVisible(IBuildingBlock block)
        {
            foreach (var socket in block.ActiveSockets)
            {
                socket.Enabled = BlockSocketManager.GetSocketTypeActive(socket.GetType());
            }
        }

        private void OnRemoveVisible(IBuildingBlock block)
        {
            foreach (var socket in block.ActiveSockets)
            {
                socket.Enabled = false;
            }
        }

        private void Awake()
        {
            enabled = BlockPreviewer.IsPreviewing;
            BlockPreviewer.BeginPreviewEvent += OnBeginPreview;
            BlockPreviewer.EndPreviewEvent += OnEndPreview;
        }

        private void OnDestroy()
        {
            BlockPreviewer.BeginPreviewEvent -= OnBeginPreview;
            BlockPreviewer.EndPreviewEvent -= OnEndPreview;
        }

        private void OnBeginPreview()
        {
            enabled = true;
        }

        private void OnEndPreview()
        {
            enabled = false;
        }

        private void OnEnable()
        {
            sqrRadius = radius * radius;
            visibleBlocks = VisibleBlocksPool.Dequeue();
            visibleBlocks.Clear();
        }

        private void OnDisable()
        {
            for (var i = 0; i < visibleBlocks.Count; ++i)
            {
                if (UnityHelpers.IsNull(visibleBlocks[i]))
                {
                    continue;
                }

                OnRemoveVisible(visibleBlocks[i]);
            }

            VisibleBlocksPool.Enqueue(visibleBlocks);
            visibleBlocks = null;
        }

        private void OnDrawGizmosSelected()
        {
            if(visibleBlocks == null)
            {
                return;
            }
            
            var here = transform.position;
            Gizmos.DrawWireSphere(transform.position, radius);

            for (var i = 0; i < visibleBlocks.Count; ++i)
            {
                if (UnityHelpers.IsNull(visibleBlocks[i]))
                {
                    continue;
                }

                Gizmos.DrawLine(here, visibleBlocks[i].Position);
            }
        }
    }
}