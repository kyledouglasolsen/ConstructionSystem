﻿using UnityEngine;

namespace ConstructionSystem
{
    public class ConstructionBehaviour : MonoBehaviour, ITransform
    {
        public Vector3 Position
        {
            get { return transform.position; }
            set { transform.position = value; }
        }

        public Quaternion Rotation
        {
            get { return transform.rotation; }
            set { transform.rotation = value; }
        }

        public Vector3 Up => transform.up;
        public Vector3 Forward => transform.forward;
        public Vector3 Right => transform.right;

        public void SetParent(ITransform newParent)
        {
            if (newParent != null)
            {
                var newParentComponent = newParent as Component;

                if (newParentComponent == null)
                {
                    Debug.LogError($"{GetType().Name} is unable to parent itself to non-component ITransform {newParent}");
                    return;
                }

                transform.SetParent(newParentComponent.transform, true);
            }
            else
            {
                transform.SetParent(null);
            }
        }

        public Vector3 TransformPoint(Vector3 position)
        {
            return transform.TransformPoint(position);
        }

        public Vector3 InverseTransformPoint(Vector3 position)
        {
            return transform.InverseTransformPoint(position);
        }

        public Vector3 TransformDirection(Vector3 direction)
        {
            return transform.TransformDirection(direction);
        }

        public Vector3 InverseTransformDirection(Vector3 direction)
        {
            return transform.InverseTransformDirection(direction);
        }
        
        public virtual void SetPositionAndRotation(Vector3 position, Quaternion rotation)
        {
            transform.SetPositionAndRotation(position, rotation);
        }
    }
}