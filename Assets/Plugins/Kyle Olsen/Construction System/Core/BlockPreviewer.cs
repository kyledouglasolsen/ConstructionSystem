﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ConstructionSystem
{
    public class BlockPreviewer : Singleton<BlockPreviewer>
    {
        public static readonly Quaternion[] RotationOffsets = {Quaternion.Euler(0f, 0f, 0f), Quaternion.Euler(0f, 90f, 0f), Quaternion.Euler(0f, 180f, 0f), Quaternion.Euler(0f, 270f, 0f)};
        public static int CurrentRotationOffsetIndex { get; private set; }

        private static IBuildingBlockPlacer currentBlockPlacer;
        private static Action<IBuildingBlock> onDeleteBlockCallback = OnDeleteBlock;
        private static HashSet<IBuildingBlockPlacer> previewBlockPlacers = new HashSet<IBuildingBlockPlacer>();
        private static List<MeshRenderer> previewRendererCache = new List<MeshRenderer>();
        private static Material validPreviewMaterial, invalidPreviewMaterial;

        public static bool IsPreviewing { get; private set; }

        public static event Action BeginPreviewEvent = delegate { };
        public static event Action EndPreviewEvent = delegate { };

        private static Material ValidPreviewMaterial
        {
            get
            {
                if (validPreviewMaterial == null)
                {
                    validPreviewMaterial = Resources.Load<Material>("Valid Placement Fallback");
                }

                return validPreviewMaterial;
            }
        }

        private static Material InvalidPreviewMaterial
        {
            get
            {
                if (invalidPreviewMaterial == null)
                {
                    invalidPreviewMaterial = Resources.Load<Material>("Invalid Placement Fallback");
                }

                return invalidPreviewMaterial;
            }
        }

        public static void SetValidPreviewMaterial(Material material)
        {
            validPreviewMaterial = material;
        }

        public static void SetInvalidPreviewMaterial(Material material)
        {
            invalidPreviewMaterial = material;
        }

        public static void ModifyRotationOffset(int direction)
        {
            if (direction > 0)
            {
                SetNextRotationOffset();
            }
            else if (direction < 0)
            {
                SetPreviousRotationOffset();
            }
        }

        public static void SetNextRotationOffset()
        {
            if (++CurrentRotationOffsetIndex >= RotationOffsets.Length)
            {
                CurrentRotationOffsetIndex = 0;
            }
        }

        public static void SetPreviousRotationOffset()
        {
            if (--CurrentRotationOffsetIndex < 0)
            {
                CurrentRotationOffsetIndex = RotationOffsets.Length - 1;
            }
        }

        public void CreatePreview<T>(IBuildingBlockPlacer blockPlacer, T prefab, ITransform parent = null) where T : IBuildingBlock
        {
            currentBlockPlacer = blockPlacer;
            var instance = prefab.Copy();
            BlockSocketManager.EnableValidSockets(instance);
            var previewBlock = BlockFactory.InitializePreview(instance, blockPlacer.BuildPosition, blockPlacer.BuildRotation);
            previewBlock.DeleteEvent -= onDeleteBlockCallback;
            previewBlock.DeleteEvent += onDeleteBlockCallback;
            previewBlock.SetParent(parent);
            previewBlockPlacers.Add(blockPlacer);
            blockPlacer.SetPreviewBlock(previewBlock);

            if (!IsPreviewing)
            {
                IsPreviewing = true;
                BeginPreviewEvent();
            }
        }

        private void LateUpdate()
        {
            if (currentBlockPlacer == null || currentBlockPlacer.PreviewBlock == null)
            {
                return;
            }

            ModifyRotationOffset((int)(Input.GetAxis("Mouse ScrollWheel") * 90f));

            var validCast = currentBlockPlacer.Cast();
            UpdateBlockPreview(currentBlockPlacer, validCast);
        }

        private static void OnDeleteBlock(IBuildingBlock block)
        {
            block.DeleteEvent -= OnDeleteBlock;
            BlockSocketManager.DisableValidSockets(block);
            var removeCount = previewBlockPlacers.RemoveWhere(x => UnityHelpers.IsNull(x) || x.PreviewBlock == block);

            if (removeCount > 0 && previewBlockPlacers.Count < 1)
            {
                IsPreviewing = false;
                EndPreviewEvent();
            }
        }

        public static void UpdateBlockPreview(IBuildingBlockPlacer placer, bool isValid)
        {
            var block = placer.PreviewBlock;
            var previewComponent = block as Component;

            if (previewComponent == null)
            {
                return;
            }

            var valid = isValid && (!placer.PreviewBlock.Data.RequireSnap || placer.SnappingSocketData.IsSnapped) && block.EvaluateConditions();
            placer.SetHasValidBuildLocation(valid);

            var meshBatch = previewComponent.GetComponent<IMeshBatch>();
            if (meshBatch != null)
            {
                Graphics.DrawMesh(meshBatch.CombineInstance.mesh, previewComponent.transform.position, previewComponent.transform.rotation, valid ? ValidPreviewMaterial : InvalidPreviewMaterial, ConstructionLayers.BuildingLayer);
            }
            else
            {
                previewRendererCache.Clear();
                previewComponent.GetComponentsInChildren(previewRendererCache);

                foreach (var meshRenderer in previewRendererCache)
                {
                    var newMaterials = meshRenderer.materials;

                    for (var i = 0; i < newMaterials.Length; ++i)
                    {
                        newMaterials[i] = valid ? ValidPreviewMaterial : InvalidPreviewMaterial;
                    }

                    meshRenderer.materials = newMaterials;
                }
            }
        }

        protected override void OnAwake()
        {
        }
    }
}