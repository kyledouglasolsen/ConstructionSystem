﻿using System.Collections.Generic;

public class StaticArrayPool<T>
{
    private readonly Dictionary<int, PooledQueue<T[]>> lookup;

    public StaticArrayPool()
    {
        lookup = new Dictionary<int, PooledQueue<T[]>>();
    }

    public T[] Get(int size)
    {
        PooledQueue<T[]> pool;

        if (!lookup.TryGetValue(size, out pool))
        {
            var arraySize = size;
            pool = new PooledQueue<T[]>(() => new T[arraySize]);
            lookup[size] = pool;
        }

        return pool.Dequeue();
    }

    public void Return(T[] array)
    {
        PooledQueue<T[]> pool;

        if (!lookup.TryGetValue(array.Length, out pool))
        {
            var arraySize = array.Length;
            pool = new PooledQueue<T[]>(() => new T[arraySize]);
            lookup[array.Length] = pool;
        }

        pool.Enqueue(array);
    }
}