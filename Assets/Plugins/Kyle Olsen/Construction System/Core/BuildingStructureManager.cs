﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ConstructionSystem
{
    public class BuildingStructureManager : Singleton<BuildingStructureManager>
    {
        private int updateIndex, lastUpdateFrame;
        private FastList<IBuildingStructure> updatingStructures = new FastList<IBuildingStructure>(10);
        public static bool EvaluateConditionsOnBlockRemove { get; private set; } = true;

        public static void SetEvaluateConditionsOnBlockRemove(bool newEvaluateConditionsOnBlocksChanged)
        {
            EvaluateConditionsOnBlockRemove = newEvaluateConditionsOnBlocksChanged;
        }

        private static uint nextBuildingStructureId;
        private static readonly Dictionary<uint, IBuildingStructure> StructureLookup = new Dictionary<uint, IBuildingStructure>();

        public static void NotifyIdSet(uint id)
        {
            if (id >= nextBuildingStructureId)
            {
                nextBuildingStructureId = id + 1;
            }
        }

        public static IBuildingStructure Create(Vector3 position)
        {
            var id = nextBuildingStructureId++;
            return FindOrCreate(position, id);
        }

        public static IBuildingStructure Create(Vector3 position, uint id)
        {
            var go = new GameObject("Building Structure: " + id);
            go.transform.position = position;
            var structure = go.AddComponent<BuildingStructure>();
            structure.SetStructureId(id);
            StructureLookup[id] = structure;
            return structure;
        }

        public static IBuildingStructure Find(uint id)
        {
            IBuildingStructure structure;

            if (StructureLookup.TryGetValue(id, out structure) && !UnityHelpers.IsNull(structure))
            {
                return structure;
            }

            return null;
        }

        public static IBuildingStructure FindOrCreate(Vector3 position, uint id)
        {
            var structure = Find(id);

            if (!UnityHelpers.IsNull(structure))
            {
                return structure;
            }

            return Create(position, id);
        }

        public static bool DestroyStructure(uint id)
        {
            IBuildingStructure structure;

            if (StructureLookup.TryGetValue(id, out structure) && DestroyStructure(structure))
            {
                Ins.updatingStructures.Remove(structure);
                return true;
            }

            return false;
        }

        public static bool DestroyStructure(IBuildingStructure structure)
        {
            if (StructureLookup.Remove(structure.StructureId))
            {
                Ins.updatingStructures.Remove(structure);

                try
                {
                    var structureComponent = structure as Component;

                    if (structureComponent != null)
                    {
                        var structureTransform = structureComponent.transform;

                        while (structureTransform.childCount > 0)
                        {
                            structureTransform.GetChild(0).SetParent(null);
                        }

                        UnityEngine.Object.Destroy(structureComponent.gameObject);

                        return true;
                    }
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }

            return false;
        }

        public static bool AddToUpdate(IBuildingStructure structure)
        {
#if UNITY_EDITOR
            if (quitting)
            {
                return false;
            }
#endif
            
            return Ins.updatingStructures.TryAdd(structure);
        }

        public static bool RemoveFromUpdate(IBuildingStructure structure)
        {
#if UNITY_EDITOR
            if (quitting)
            {
                return false;
            }
#endif
            
            return Ins.updatingStructures.Remove(structure);
        }

        protected override void OnAwake()
        {
        }
        
        private void FixedUpdate()
        {
            var thisFrame = Time.frameCount;
            
            if(thisFrame == lastUpdateFrame)
            {
                return;
            }
            
            lastUpdateFrame = thisFrame;
            
            if(updatingStructures.Count < 1)
            {
                enabled = false;
                return;
            }
            
            if(updateIndex >= updatingStructures.Count)
            {
                updateIndex = 0;
            }
            
            for(; updateIndex < updatingStructures.Count; ++updateIndex)
            {
                var structure = updatingStructures[updateIndex];
                
                if(UnityHelpers.IsNull(structure))
                {
                    continue;
                }
                
                structure.UpdateEvaluationList();
                
                break;
            }
        }
    }
}