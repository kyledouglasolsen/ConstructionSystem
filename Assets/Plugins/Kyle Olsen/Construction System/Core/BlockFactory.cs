﻿using ConstructionSystem.BuildingSockets;
using UnityEngine;

namespace ConstructionSystem
{
    public class BlockFactory
    {
        public static IBuildingBlock InitializePreview(IBuildingBlock block, Vector3 position, Quaternion rotation)
        {
            if (block.IsInitialized)
            {
                Debug.LogError("Initialized block preview passed to BlockFactory for creation!", block as Component);
                return block;
            }

            block.SetPositionAndRotation(position, rotation);
            block.Initialize(true);

            return block;
        }

        public static IBuildingBlock Initialize(IBuildingBlock block, IBuildingSocket snappingToSocket, Vector3 position, Quaternion rotation)
        {
            if (block.IsInitialized)
            {
                Debug.LogError("Attempting to initialize block that is already initialized!", block as Component);
                return block;
            }

            var structure = snappingToSocket?.OwningBlock?.Structure;

            if (UnityHelpers.IsNull(structure))
            {
                structure = BuildingStructureManager.Create(position);
            }

            return Initialize(block, structure, position, rotation);
        }

        public static IBuildingBlock Initialize(IBuildingBlock block, uint structureId, Vector3 position, Quaternion rotation)
        {
            var structure = BuildingStructureManager.Find(structureId);

            if (UnityHelpers.IsNull(structure))
            {
                structure = BuildingStructureManager.Create(position, structureId);
            }

            return Initialize(block, structure, position, rotation);
        }

        public static IBuildingBlock Initialize(IBuildingBlock block, IBuildingStructure structure, Vector3 position, Quaternion rotation)
        {
            if (block.IsInitialized)
            {
                Debug.LogError("Attempting to initialize block that is already initialized!", block as Component);
                return block;
            }

            block.SetPositionAndRotation(position, rotation);
            structure.AddBlock(block);
            block.Initialize(false);
            return block;
        }
    }
}