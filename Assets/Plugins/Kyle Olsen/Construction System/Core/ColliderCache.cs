﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class ColliderCache
{
    private static Dictionary<Type, CacheColliderData> colliderLookup = new Dictionary<Type, CacheColliderData>();

    public static T Get<T>(int layer, bool isTrigger, Transform parent = null) where T : Collider
    {
        return Get<T>(layer, Vector3.zero, Quaternion.identity, Vector3.one, isTrigger, parent);
    }

    public static T Get<T>(int layer, Vector3 localPosition, Quaternion localRotation, Vector3 localScale, bool isTrigger, Transform parent = null) where T : Collider
    {
        var type = typeof(T);

        CacheColliderData cache;

        if (!colliderLookup.TryGetValue(type, out cache))
        {
            cache = new CacheColliderData(type);
            colliderLookup[type] = cache;
        }

        return cache.Dequeue<T>(layer, localPosition, localRotation, localScale, isTrigger, parent);
    }

    public static void Return(Collider collider)
    {
        if (collider == null)
        {
            return;
        }

        var type = collider.GetType();
        colliderLookup[type].Enqueue(collider);
    }

    private class CacheColliderData
    {
        private Func<ColliderQueueData> createColliderCallback;
        private PooledQueue<ColliderQueueData> pool;
        private static FastList<Collider> active = new FastList<Collider>();

        public CacheColliderData(Type type)
        {
            if (active == null)
            {
                active = new FastList<Collider>(50);
            }

            createColliderCallback = () =>
            {
                var go = new GameObject(type.Name + " (Cached)");
                return new ColliderQueueData(-1, (Collider)go.AddComponent(type));
            };

            pool = new PooledQueue<ColliderQueueData>(createColliderCallback);
        }

        public void Enqueue(Collider collider)
        {
            if (active.Contains(collider))
            {
                active.Remove(collider);
                pool.Enqueue(new ColliderQueueData(Time.frameCount + 10, collider));
                collider.gameObject.SetActive(false);
                ZeroCollider(collider);
            }
        }

        public T Dequeue<T>(int layer, Vector3 localPosition, Quaternion localRotation, Vector3 localScale, bool isTrigger, Transform parent = null) where T : Collider
        {
            var data = new ColliderQueueData();

            while (data.Collider == null)
            {
                data = pool.Dequeue();

                if (Time.frameCount <= data.Frame)
                {
                    pool.Enqueue(data);
                    data = createColliderCallback();
                    break;
                }
            }

            data.Collider.transform.SetParent(parent);
            data.Collider.transform.localPosition = localPosition;
            data.Collider.transform.localRotation = localRotation;
            data.Collider.transform.localScale = localScale;

            if(data.Collider.isTrigger != isTrigger)
            {
                data.Collider.isTrigger = isTrigger;
            }

            data.Collider.gameObject.layer = layer;
            data.Collider.gameObject.SetActive(true);

            active.Add(data.Collider);

            return data.Collider as T;
        }

        private static void ZeroCollider(Collider collider)
        {
            collider.isTrigger = false;

            var boxCollider = collider as BoxCollider;
            if(boxCollider != null)
            {
                boxCollider.center = Vector3.zero;
                boxCollider.size = Vector3.one;
            }

            var sphere = collider as SphereCollider;
            if(sphere != null)
            {
                sphere.center = Vector3.zero;
                sphere.radius = 1f;
            }

            var capsule = collider as CapsuleCollider;
            if(capsule != null)
            {
                capsule.center = Vector3.zero;
                capsule.height = 1f;
                capsule.radius = 0.5f;
            }
        }
    }

    private struct ColliderQueueData
    {
        public ColliderQueueData(int frame, Collider collider)
        {
            Frame = frame;
            Collider = collider;
        }

        public int Frame { get; }
        public Collider Collider { get; }
    }
}