﻿using UnityEngine;

namespace ConstructionSystem
{
    public class DefaultMeshBatch : BlockMeshBatch
    {
        [SerializeField] private Material material;
        [SerializeField] private Mesh visualMesh;

        protected override bool TryGatherBatchData()
        {
            if (visualMesh == null)
            {
                return false;
            }
            
            Material = material;
            
            var parent = transform.parent;
            Matrix4x4 matrix;
            
            if (parent != null)
            {
                matrix = parent.worldToLocalMatrix * transform.localToWorldMatrix;
            }
            else
            {
                matrix = transform.localToWorldMatrix;
            }
            
            CombineInstance = new CombineInstance() {mesh = visualMesh, transform = matrix};

            return true;
        }

#if UNITY_EDITOR
        private void Reset()
        {
            var childRenderer = GetComponentInChildren<MeshRenderer>(true);
            if (childRenderer != null)
            {
                material = childRenderer.sharedMaterial;
            }

            var childFilter = GetComponentInChildren<MeshFilter>(true);
            if (childFilter != null)
            {
                visualMesh = childFilter.sharedMesh;
            }
        }
#endif
    }
}