﻿using UnityEngine;

namespace ConstructionSystem
{
    public abstract class BlockPlacer : IBuildingBlockPlacer
    {
        public IBuildingBlock PreviewBlock { get; private set; }
        public SocketFinder.FoundSocket SnappingSocketData { get; private set; }
        public bool HasValidBuildLocation { get; private set; }
        public Vector3 BuildPosition { get; private set; }
        public Quaternion BuildRotation { get; private set; }

        public void SetHasValidBuildLocation(bool hasValidBuildLocation)
        {
            HasValidBuildLocation = hasValidBuildLocation;
        }

        public void SetPosition(Vector3 position)
        {
            BuildPosition = position;
        }

        public void SetRotation(Quaternion rotation)
        {
            BuildRotation = rotation;
        }

        public void SetSnappingData(SocketFinder.FoundSocket snappingSocketData)
        {
            SnappingSocketData = snappingSocketData;
        }

        public bool Cast()
        {
            var validCast = OnCast();
            BlockPreviewer.UpdateBlockPreview(this, validCast);
            return validCast;
        }

        public virtual void SetPreviewBlock(IBuildingBlock previewBlock)
        {
            PreviewBlock = previewBlock;
            OnSetPreviewBlock(previewBlock);
        }

        public void DeletePreviewBlock()
        {
            if (PreviewBlock != null)
            {
                PreviewBlock.Delete(true);
            }

            PreviewBlock = null;
        }

        protected abstract bool OnCast();
        protected abstract void OnSetPreviewBlock(IBuildingBlock previewBlock);
    }
}