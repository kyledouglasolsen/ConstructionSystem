﻿using System;
using System.Collections.Generic;
using ConstructionSystem.BuildingSockets;
using UnityEngine;

namespace ConstructionSystem
{
    public static class SocketFinder
    {
        private static readonly List<SortedSocket> SortedSockets = new List<SortedSocket>();
        private static readonly Comparison<SortedSocket> SortedSocketComparer = (a, b) => a.Weight > b.Weight ? 1 : -1;

        public static FoundSocket FindValid(IBuildingBlock previewBlock, Ray ray, RaycastHit[] hits, int count)
        {
            return FindValid(previewBlock, ray, hits, count, new FoundSocket(null, false));
        }

        public static FoundSocket FindValid(IBuildingBlock previewBlock, Ray ray, RaycastHit[] hits, int count, FoundSocket activeSocket)
        {
            if (previewBlock == null)
            {
                return new FoundSocket();
            }

            var previewBlockStartingRotation = previewBlock.Rotation;

            // Check last hit socket for valid placement
            if (activeSocket.HitSocket != null && activeSocket.IsSnapped)
            {
                var hitActiveSocket = false;

                for (var i = 0; i < count; ++i)
                {
                    var hit = hits[i];
                    var hitSocket = hit.collider.GetComponentInParent<IBuildingSocket>();
                    if (hitSocket != null && hitSocket == activeSocket.HitSocket)
                    {
                        hitActiveSocket = true;
                        break;
                    }
                }

                if (hitActiveSocket && activeSocket.HitSocket.IsValidConnection(previewBlock))
                {
                    var isSnapped = activeSocket.HitSocket.SnapBlockTo(previewBlock, previewBlock.Position, previewBlockStartingRotation);
                    
                    if (isSnapped && previewBlock.EvaluateConditions())
                    {
                        return new FoundSocket(activeSocket.HitSocket, true);
                    }
                }
            }

            // Find best valid socket where all conditions are also met
            for (var i = 0; i < count; ++i)
            {
                var hit = hits[i];
                var hitSocket = hit.collider.GetComponentInParent<IBuildingSocket>();

                if (hitSocket == null || !hitSocket.IsValidConnection(previewBlock))
                {
                    continue;
                }

                var isSnapped = hitSocket.SnapBlockTo(previewBlock, hit.point, previewBlockStartingRotation);

                if (!isSnapped || !previewBlock.EvaluateConditions())
                {
                    continue;
                }

                var snappedCenter = previewBlock.Bounds.center;
                var distanceToBlock = (snappedCenter - ray.origin).sqrMagnitude;
                var snappedDot = Vector3.Dot(ray.direction, (snappedCenter - ray.origin).normalized);
                var overBounds = previewBlock.Bounds.IntersectRay(ray);
                var weight = (distanceToBlock) * (1f - Mathf.Clamp01(snappedDot)) * (overBounds ? 0.1f : 1f);
                SortedSockets.Add(new SortedSocket(hitSocket, weight));
            }

            SortedSockets.Sort(SortedSocketComparer);

            if (SortedSockets.Count > 0)
            {
                var foundSocket = new FoundSocket(SortedSockets[0].HitSocket, true);
                SortedSockets.Clear();
                return foundSocket;
            }

            // Just find a valid socket
            for (var i = 0; i < count; ++i)
            {
                var hit = hits[i];
                var hitSocket = hit.collider.GetComponentInParent<IBuildingSocket>();

                if (hitSocket == null || !hitSocket.IsValidConnection(previewBlock))
                {
                    continue;
                }

                var snapResults = hitSocket.SnapBlockTo(previewBlock, hit.point, previewBlockStartingRotation);
                return new FoundSocket(hitSocket, snapResults);
            }

            return new FoundSocket();
        }

        public struct FoundSocket
        {
            public FoundSocket(IBuildingSocket hitSocket, bool isSnapped) : this()
            {
                HitSocket = hitSocket;
                IsSnapped = isSnapped;
            }

            public IBuildingSocket HitSocket { get; }
            public bool IsSnapped { get; }
        }

        private struct SortedSocket
        {
            public SortedSocket(IBuildingSocket hitSocket, float weight)
            {
                HitSocket = hitSocket;
                Weight = weight;
            }

            public IBuildingSocket HitSocket { get; }
            public float Weight { get; }
        }
    }
}