﻿using ConstructionSystem;

public class DefaultBlockCreator : IBuildingBlockCreator
{
    public bool TrySpawn(IBuildingBlock prefab, IBuildingBlockPlacer placer)
    {
        if (!placer.PreviewBlock.Conditions.Evaluate(placer.PreviewBlock))
        {
            return false;
        }

        var instance = prefab.Copy();
        BlockFactory.Initialize(instance, placer.SnappingSocketData.HitSocket, placer.PreviewBlock.Position, placer.PreviewBlock.Rotation);
        return true;
    }
}
