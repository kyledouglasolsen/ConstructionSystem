﻿using System;
using System.Collections;
using System.Collections.Generic;
using ConstructionSystem.Conditions;
using UnityEngine;

namespace ConstructionSystem
{
    public class BuildingStructure : ConstructionBehaviour, IBuildingStructure
    {
        private static readonly PooledQueue<List<IBuildingBlock>> OverlapCache = new PooledQueue<List<IBuildingBlock>>(() => new List<IBuildingBlock>(50));

#if UNITY_EDITOR
        [SerializeField] private bool debugBlockStability = false;
#endif
        private int structureSteadyFrameCount, targetSteadyFrameCount;
        private HashSet<IBuildingBlock> blocks = new HashSet<IBuildingBlock>();
        private FastList<IBuildingBlock> evaluationList = new FastList<IBuildingBlock>(10);
        private BoundsOctree<IBuildingBlock> octree;

        public uint StructureId { get; private set; }
        public IBatchMeshes MeshBatcher { get; private set; }

        public void SetStructureId(uint structureId)
        {
            StructureId = structureId;
            BuildingStructureManager.NotifyIdSet(structureId);
        }

        public void EvaluateStabilityFrom(IBuildingBlock block)
        {
            if (UnityHelpers.IsNull(block))
            {
                return;
            }

            AddNeighborBlocksToEvaluationList(block);
        }

        public bool AddBlock(IBuildingBlock block)
        {
            if (!blocks.Add(block))
            {
                return false;
            }

            octree.Add(block, block.Bounds);
            block.SetStructure(this);
            block.SetParent(this);

            return true;
        }

        public bool RemoveBlock(IBuildingBlock block)
        {
            var removed = blocks.Remove(block);

            if (removed)
            {
                octree.Remove(block);

                if (BuildingStructureManager.EvaluateConditionsOnBlockRemove)
                {
                    AddNeighborBlocksToEvaluationList(block);
                }
            }

            return removed;
        }

        private void AddNeighborBlocksToEvaluationList(IBuildingBlock block)
        {
            var cache = OverlapCache.Dequeue();
            OverlapBoxNonAlloc(block.Bounds, ref cache, 1.1f);

            for (var i = 0; i < cache.Count; ++i)
            {
                evaluationList.Add(cache[i]);
            }

            if (cache.Count > 0)
            {
                structureSteadyFrameCount = 0;
                
                if(evaluationList.ManagedCount > 0)
                {
                    BuildingStructureManager.AddToUpdate(this);
                }
                else
                {
                    BuildingStructureManager.RemoveFromUpdate(this);
                }
            }

            cache.Clear();
            OverlapCache.Enqueue(cache);
        }

        public IBuildingBlock GetBlockFromPoint(Vector3 point)
        {
            List<IBuildingBlock> cache = null;

            try
            {
                cache = OverlapCache.Dequeue();

                var bounds = new Bounds(point, Vector3.zero);
                OverlapBoxNonAlloc(bounds, ref cache, 1f);

                var closestIndex = -1;
                var closestDistance = float.MaxValue;

                for (var i = 0; i < cache.Count; ++i)
                {
                    var overlapBlock = cache[i];

                    if (UnityHelpers.IsNull(overlapBlock))
                    {
                        continue;
                    }

                    if (overlapBlock.Bounds.Contains(point))
                    {
                        return overlapBlock;
                    }

                    var sqrDistance = (overlapBlock.Bounds.ClosestPoint(point) - point).sqrMagnitude;

                    if (sqrDistance < closestDistance)
                    {
                        closestDistance = sqrDistance;
                        closestIndex = i;
                    }
                }

                IBuildingBlock foundBlock = null;

                if (closestIndex != -1)
                {
                    foundBlock = cache[closestIndex];
                }

                return foundBlock;
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
            finally
            {
                cache.Clear();
                OverlapCache.Enqueue(cache);
            }

            return null;
        }

        public void OverlapBoxNonAlloc(Bounds bounds, ref List<IBuildingBlock> cachedOverlapBlocks, float scaleSize = 1f)
        {
            bounds = new Bounds(bounds.center, bounds.size * scaleSize);
            octree.GetColliding(cachedOverlapBlocks, bounds);
        }

        public IBatchCollision FindNeighborBatch<T>(IBuildingBlock block) where T : IBatchCollision, IHasBounds
        {
            if (block.IsPreview)
            {
                return null;
            }

            List<IBuildingBlock> cache = null;

            try
            {
                cache = OverlapCache.Dequeue();

                var searchBatch = block as IBatchCollision;
                var bounds = (block.Collider as IBlockBatch<IBuildingBlock> ?? (IHasBounds)block).Bounds;
                OverlapBoxNonAlloc(bounds, ref cache, 1.1f);

                for (var i = 0; i < cache.Count; ++i)
                {
                    var overlapBlock = cache[i];

                    if (UnityHelpers.IsNull(overlapBlock))
                    {
                        continue;
                    }

                    var batch = overlapBlock as IBatchCollision;
                    var blockBatch = batch?.BlockBatch;

                    if (blockBatch == null)
                    {
                        continue;
                    }

                    if (searchBatch != null)
                    {
                        if (blockBatch == searchBatch.BlockBatch)
                        {
                            continue;
                        }
                    }

                    if (!blockBatch.TestForCompatibility<T>(block))
                    {
                        continue;
                    }

                    return batch;
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
            finally
            {
                cache.Clear();
                OverlapCache.Enqueue(cache);
            }

            return null;
        }

        private void Awake()
        {
            octree = new BoundsOctree<IBuildingBlock>(1f, transform.position, 1f, 0.1f);
            MeshBatcher = new StructureMeshBatcher(this);
        }

        public bool UpdateEvaluationList()
        {
            if (evaluationList.ManagedCount < 1)
            {
                BuildingStructureManager.RemoveFromUpdate(this);

                if (blocks.Count < 1)
                {
                    BuildingStructureManager.DestroyStructure(this);
                }

                return false;
            }

            var foundInvalidBlock = false;

            for (var i = evaluationList.Count - 1; i >= 0; --i)
            {
                var block = evaluationList[i];
                var isNull = UnityHelpers.IsNull(block);

                if (isNull)
                {
                    continue;
                }

                IConstructionCondition failedCondition;
                var validConditions = block.EvaluateConditions(out failedCondition);

                if (!validConditions)
                {
                    blocks.Remove(block);
                    octree.Remove(block);
                    BlockPhysics.Ins.Add(block);
                    AddNeighborBlocksToEvaluationList(block);
                    evaluationList.Remove(block);
                    structureSteadyFrameCount = 0;
                    foundInvalidBlock = true;

#if UNITY_EDITOR
                    if (debugBlockStability)
                    {
                        Debug.LogError($"Block {block} lost stability from condition {failedCondition}");
                    }
#endif
                }
            }

            if (!foundInvalidBlock)
            {
                ++structureSteadyFrameCount;
            }

            if (structureSteadyFrameCount > Mathf.Max(3, evaluationList.ManagedCount * 0.1f))
            {
                BuildingStructureManager.RemoveFromUpdate(this);
                evaluationList.Clear();

                if (blocks.Count < 1)
                {
                    BuildingStructureManager.DestroyStructure(this);
                }
                
                return false;
            }
            
            return true;
        }

        public IEnumerator<IBuildingBlock> GetEnumerator()
        {
            return blocks.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}