﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ConstructionSystem
{
    public class BlockPhysics : Singleton<BlockPhysics>
    {
        private const float InsetDistance = 0.001f;
        private static bool useRigidbodyBlockPhysics = true;
        private static bool destroyBlockOnStabilityLoss = true;

        public static void SetRigidbodyBlockPhysics(bool newUseRigidbodyBlockPhysics)
        {
            useRigidbodyBlockPhysics = newUseRigidbodyBlockPhysics;
        }

        public static void SetDestroyBlockOnStabilityLoss(bool newDestroyBlockOnStabilityLoss)
        {
            destroyBlockOnStabilityLoss = newDestroyBlockOnStabilityLoss;
        }

        public static IBuildingBlock SphereCast(Ray ray, float radius, float distance, LayerMask layerMask, QueryTriggerInteraction queryTriggerInteraction)
        {
            return SphereCast(ray, radius, distance, layerMask, queryTriggerInteraction, RaycastDistanceComparer.Instance);
        }

        public static IBuildingBlock SphereCast(Ray ray, float radius, float distance, LayerMask layerMask, QueryTriggerInteraction queryTriggerInteraction, IComparer<RaycastHit> raycastComparer)
        {
            var hits = Physics.SphereCastAll(ray, radius, distance, layerMask, QueryTriggerInteraction.Ignore);

            if (raycastComparer != null)
            {
                Array.Sort(hits, raycastComparer);
            }

            for (var i = 0; i < hits.Length; ++i)
            {
                var hit = hits[i];
                var block = GetBlockFromRaycastHit(ray, hit);

                if (block != null)
                {
                    return block;
                }
            }

            return null;
        }

        public static IBuildingBlock GetBlockFromRaycastHit(Ray ray, RaycastHit hit)
        {
            if (hit.collider == null)
            {
                return null;
            }

            var buildingBlock = hit.collider.GetComponentInParent<IBuildingBlock>();

            if (buildingBlock == null)
            {
                return null;
            }

            var batchCollision = buildingBlock as IBatchCollision;

            if (batchCollision != null && batchCollision.IsBatched && batchCollision.BlockBatch != null)
            {
                buildingBlock = batchCollision.BlockBatch.GetBlockFromPoint(hit.point + ray.direction * InsetDistance);
            }

            return buildingBlock;
        }

        public void Add(IBuildingBlock block)
        {
            Add(block, destroyBlockOnStabilityLoss);
        }

        public void Add(IBuildingBlock block, bool destroyGo)
        {
            var component = block as Component;

            if (component == null)
            {
                return;
            }

            Rigidbody blockRigidbody = null;
            var batch = block as IBatchCollision;
            var blockPhysics = block as IBlockPhysics;
            var removedBatch = batch != null && batch.IsBatched && BatchedWorldCollision.Remove(block);
            block.Structure?.RemoveBlock(block);
            block.ActiveSockets?.DeleteAll();

            if (useRigidbodyBlockPhysics && blockPhysics != null)
            {
                blockPhysics.SetSimulatingPhysics(true);

                if (removedBatch)
                {
                    block.SetCollider(block.Data.ColliderData.ApplyTo(block));
                }

                component.gameObject.layer = ConstructionLayers.BlockPhysicsLayer;

                if (block.Collider != null)
                {
                    for(var i = 0; i < block.Collider.Count; ++i)
                    {
                        var col = block.Collider[i];
                        
                        if (col == null)
                        {
                            continue;
                        }

                        col.gameObject.layer = ConstructionLayers.BlockPhysicsLayer;

                        if (col.isTrigger)
                        {
                            col.isTrigger = false;
                        }
                    }
                }

                blockRigidbody = component.GetOrAddComponent<Rigidbody>();
                blockRigidbody.isKinematic = false;
            }

            var blockDeleted = false;
            Action<IBuildingBlock> deleteCallback = null;

            deleteCallback = (deletedBlock) =>
            {
                blockDeleted = true;

                if (block.Collider != null)
                {
                    block.Collider.ReturnToCache();
                    block.SetCollider(null);
                }

                if (blockPhysics != null)
                {
                    blockPhysics.SetSimulatingPhysics(false);
                }

                if (blockRigidbody != null)
                {
                    blockRigidbody.isKinematic = true;
                }

                block.DeleteEvent -= deleteCallback;
            };

            block.DeleteEvent += deleteCallback;

            Timed.In(10f, () =>
            {
                if (blockDeleted)
                {
                    return;
                }

                if (blockRigidbody != null)
                {
                    blockRigidbody.isKinematic = true;
                }

                block.Delete(destroyGo);
            });
        }

        protected override void OnAwake()
        {
        }
    }
}