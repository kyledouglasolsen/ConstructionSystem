﻿using UnityEngine;

namespace ConstructionSystem
{
    public static class ConstructionLayers
    {
        public static int TerrainLayer = LayerMask.NameToLayer("Terrain");
        public static int BlockPhysicsLayer = LayerMask.NameToLayer("BlockPhysics");
        public static int BuildingLayer = LayerMask.NameToLayer("BuildingBlock");
        public static int BuildingSocketLayer = LayerMask.NameToLayer("BuildingSocket");

        public static LayerMask DefaultPlacerLayerMask = (1 << TerrainLayer | 1 << BuildingSocketLayer | 1 << BuildingLayer);
        public static LayerMask BatchedColliderOverlapLayerMask = (1 << BuildingLayer);
    }
}
