﻿//using UnityEngine;
//using UnityEngine.Assertions;
//
//#if UNITY_EDITOR
//
//namespace ConstructionSystem.Tests
//{
//    public class StaticBoundsContainsTest : MonoBehaviour
//    {
//        [SerializeField] private Vector3 testPoint = Vector3.zero;
//
//        [SerializeField] private Vector3 bounds1Center = Vector3.zero;
//        [SerializeField] private Vector3 bounds1Size = Vector3.one;
//
//        private void Start()
//        {
//            var bounds = new StaticBounds(bounds1Center, bounds1Size);
//            Debug.Log(bounds.Contains(testPoint));
//            Assert.IsTrue(bounds.Contains(testPoint));
//        }
//
//        private void OnDrawGizmosSelected()
//        {
//            var bounds = new StaticBounds(bounds1Center, bounds1Size);
//            Gizmos.DrawWireCube(bounds.Center, bounds.Size);
//
//            Gizmos.color = bounds.Contains(testPoint) ? Color.red : Color.white;
//            Gizmos.DrawWireSphere(testPoint, 0.1f);
//        }
//    }
//}
//
//#endif