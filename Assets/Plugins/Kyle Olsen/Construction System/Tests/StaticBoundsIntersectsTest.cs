﻿//using UnityEngine;
//using UnityEngine.Assertions;
//
//#if UNITY_EDITOR
//
//namespace ConstructionSystem.Tests
//{
//    public class StaticBoundsIntersectsTest : MonoBehaviour
//    {
//        [SerializeField] private Vector3 bounds1Center = Vector3.zero;
//        [SerializeField] private Vector3 bounds1Size = Vector3.one;
//        [SerializeField] private Vector3 bounds2Center = Vector3.right;
//        [SerializeField] private Vector3 bounds2Size = Vector3.one;
//
//        private void Start()
//        {
//            var bounds1 = new StaticBounds(bounds1Center, bounds1Size);
//            var bounds2 = new StaticBounds(bounds2Center, bounds2Size);
//            Debug.Log(bounds1.Intersects(bounds2));
//            Assert.IsTrue(bounds1.Intersects(bounds2));
//        }
//
//        private void OnDrawGizmosSelected()
//        {
//            var bounds1 = new StaticBounds(bounds1Center, bounds1Size);
//            var bounds2 = new StaticBounds(bounds2Center, bounds2Size);
//            Gizmos.DrawWireCube(bounds1.Center, bounds1.Size);
//            Gizmos.DrawWireCube(bounds2.Center, bounds2.Size);
//        }
//    }
//}
//
//#endif