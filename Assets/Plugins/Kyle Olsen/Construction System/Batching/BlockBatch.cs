﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if DEBUG_CONSTRUCTION_COLLISION
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

#endif

namespace ConstructionSystem
{
    public abstract class BlockBatch<T> : IBlockBatch<T> where T : IBuildingBlock
    {
        public const float Epsilon = 0.01f;

        private Bounds bounds;
        private Collider batchedCollider;
        private bool rebatching;

        public Quaternion Rotation { get; }

        public FastList<IBuildingBlock> Blocks { get; }
        public Bounds Bounds => bounds;
        public int Count => 1;
        public Collider this[int index] => batchedCollider;

        protected BlockBatch(T block, Quaternion rotation)
        {
            Blocks = new FastList<IBuildingBlock>();
            Rotation = rotation;
            Blocks.Add(block);

            bounds = block.Bounds;
            UpdateCollider();
        }

        protected BlockBatch(FastList<IBuildingBlock> blocks, Quaternion rotation)
        {
            Blocks = blocks;
            Rotation = rotation;

            foreach (var block in blocks)
            {
                this.Blocks.Add(block);
            }

            UpdateBounds();
            UpdateCollider();
        }

        public void ForceUpdateBounds()
        {
            UpdateBounds();
        }

        public bool Add(IBuildingBlock block, bool updateCollider)
        {
            var added = Blocks.TryAdd(block);

            if (added)
            {
                UpdateBounds();

                if (updateCollider)
                {
                    UpdateCollider();
                }
            }

            return added;
        }

        public bool Remove(IBuildingBlock block, bool updateCollider)
        {
            var removed = Blocks.Remove(block);

            if (removed)
            {
                UpdateBounds();

                if (updateCollider)
                {
                    UpdateCollider();
                }
            }

            return removed;
        }

        public bool Contains(IBuildingBlock block)
        {
            return Blocks.Contains(block);
        }

        public void MergeWith(IBlockBatch<IBuildingBlock> otherBatch)
        {
            foreach (var block in Blocks)
            {
                if (UnityHelpers.IsNull(block))
                {
                    continue;
                }

                otherBatch.Add(block, false);
                block.SetCollider(otherBatch);
            }

            Dispose();
            otherBatch.UpdateCollider();
        }

        public void UpdateCollider()
        {
            ReturnToCache();

            if (Blocks.Count <= 0)
            {
                return;
            }

            var mainBlock = Blocks.First();
            var mainComponent = mainBlock as Component;

            if (mainComponent == null)
            {
                return;
            }

            var transform = mainComponent.transform;
            var localPosition = transform.InverseTransformPoint(Bounds.center);
            batchedCollider = ColliderCache.Get<BoxCollider>(ConstructionLayers.BuildingLayer, localPosition, Quaternion.Inverse(Rotation), Bounds.size, false, transform);

#if DEBUG_CONSTRUCTION_COLLISION
            CreateOrUpdateDebugMesh();
#endif
        }

        public void Rebatch()
        {
            if (rebatching)
            {
                return;
            }

            rebatching = true;

            Timed.NextFixedFrame(() =>
            {
                ReturnToCache();

                foreach (var block in Blocks)
                {
                    if (UnityHelpers.IsNull(block))
                    {
                        continue;
                    }

                    var mainBlockBatch = (IBatchCollision)block;
                    var newBatch = mainBlockBatch.CreateNewBatch();
                    block.SetCollider(newBatch);
                }

                foreach (var block in Blocks)
                {
                    if (UnityHelpers.IsNull(block))
                    {
                        continue;
                    }

                    BatchedWorldCollision.Rebatch((IBatchCollision)block);
                }
            });
        }

        public IBuildingBlock GetBlockFromPoint(Vector3 point)
        {
            foreach (var block in Blocks)
            {
                if (UnityHelpers.IsNull(block))
                {
                    continue;
                }

                if (block.Bounds.Contains(point))
                {
                    return block;
                }
            }

            return default(T);
        }

        public void OverlapBoxNonAlloc(Bounds testBounds, ref List<IBuildingBlock> cachedOverlapBlocks, float scaleSize = 1f)
        {
            testBounds = new Bounds(testBounds.center, testBounds.size * scaleSize);

            foreach (var block in Blocks)
            {
                if (UnityHelpers.IsNull(block))
                {
                    continue;
                }

                if (block.Bounds.Intersects(testBounds))
                {
                    cachedOverlapBlocks.Add(block);
                }
            }
        }

        public abstract bool TestForCompatibility<TS>(IBuildingBlock block) where TS : IBatchCollision, IHasBounds;
        public abstract IBlockBatch<T> CreateNewBatch(FastList<IBuildingBlock> newBlocks, Quaternion rotation);

        private void UpdateBounds()
        {
            var first = true;

            foreach (var block in Blocks)
            {
                if (UnityHelpers.IsNull(block))
                {
                    continue;
                }

                if (first)
                {
                    bounds = block.Bounds;
                    first = false;
                }
                else
                {
                    bounds.Encapsulate(block.Bounds);
                }
            }

            if (first)
            {
                bounds = new Bounds();
            }
        }

        public Collider Main()
        {
            return batchedCollider;
        }

        public void ReturnToCache()
        {
            if (batchedCollider != null)
            {
                ColliderCache.Return(batchedCollider);
            }

#if DEBUG_CONSTRUCTION_COLLISION
            if (debugObject != null)
            {
                Object.Destroy(debugObject);
            }
#endif
        }

        public void Dispose()
        {
            ReturnToCache();
            Blocks.Clear();
        }

#if DEBUG_CONSTRUCTION_COLLISION

        private const string DebugMeshName = "[Construction System] Debug Mesh";
        private static readonly Dictionary<System.Type, Mesh> primitiveMeshLookup = new Dictionary<System.Type, Mesh>();
        private GameObject debugObject;
        private Material material;

        public void CreateOrUpdateDebugMesh()
        {
            if (debugObject != null)
            {
                Object.Destroy(debugObject);
            }

            if (Blocks.Count > 0)
            {
                var mainBlock = Blocks.First();
                var mainComponent = mainBlock as Component;

                if (mainComponent != null)
                {
                    var transform = mainComponent.transform;
                    var localPosition = transform.InverseTransformPoint(Bounds.center);
                    debugObject = new GameObject(DebugMeshName);
                    debugObject.transform.SetParent(transform);
                    debugObject.transform.localPosition = localPosition;
                    debugObject.transform.localRotation = Quaternion.Inverse(Rotation);
                    debugObject.transform.localScale = Bounds.size;
                    var debugRenderer = debugObject.AddComponent<MeshRenderer>();

                    if (material == null)
                    {
                        material = new Material(Shader.Find("Unlit/Color")) {color = new Color(Random.value, Random.value, Random.value, 1f)};
                    }

                    debugRenderer.sharedMaterial = material;
                    var debugFilter = debugObject.AddComponent<MeshFilter>();
                    debugFilter.sharedMesh = GetPrimitiveMesh(typeof(BoxCollider));
                }
            }
        }

        public static void DestroyDebugMesh(BoxCollider collider)
        {
            while (collider.transform.childCount > 0)
            {
                Object.DestroyImmediate(collider.transform.GetChild(0).gameObject);
            }
        }

        private static Mesh GetPrimitiveMesh(System.Type type)
        {
            if (primitiveMeshLookup.ContainsKey(type))
            {
                return primitiveMeshLookup[type];
            }

            GameObject debugGo;

            if (type == typeof(BoxCollider))
            {
                debugGo = GameObject.CreatePrimitive(PrimitiveType.Cube);
            }
            else if (type == typeof(SphereCollider))
            {
                debugGo = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            }
            else if (type == typeof(CapsuleCollider))
            {
                debugGo = GameObject.CreatePrimitive(PrimitiveType.Capsule);
            }
            else
            {
                Debug.LogError($"Unknown primitive mesh type: {type}");
                return null;
            }

            var mesh = debugGo.GetComponent<MeshFilter>().sharedMesh;
            primitiveMeshLookup[type] = mesh;

            return mesh;
        }

#endif
    }
}