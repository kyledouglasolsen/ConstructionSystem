﻿namespace ConstructionSystem
{
    public static class BatchedWorldCollision
    {
        public static void Add(IBuildingBlock block)
        {
            var batch = block as IBatchCollision;

            if (batch != null)
            {
                var blockBatch = batch.BlockBatch;

                if (blockBatch == null)
                {
                    var neighbor = batch.FindNeighborBatch();

                    if (neighbor != null && neighbor.IsBatched)
                    {
                        blockBatch = neighbor.BlockBatch;
                    }
                }

                if (blockBatch == null)
                {
                    blockBatch = batch.CreateNewBatch();
                }

                blockBatch.Add(block, true);
                block.SetCollider(blockBatch);
                Rebatch(batch);
            }
            else
            {
                block.SetCollider(block.Data.ColliderData.ApplyTo(block));
            }
        }

        public static bool Remove(IBuildingBlock block)
        {
            var batch = block as IBatchCollision;

            if (batch != null)
            {
                var blockBatch = batch.BlockBatch;

                if (blockBatch != null)
                {
                    if (blockBatch.Remove(block, true))
                    {
                        blockBatch.Rebatch();
                        block.SetCollider(null);
                        return true;
                    }
                }
            }

            block.Collider?.ReturnToCache();
            block.SetCollider(null);
            return false;
        }

        public static void Rebatch(IBatchCollision batch)
        {
            var block = (IBuildingBlock)batch;
            var blockBatch = batch.BlockBatch;
            
            if(blockBatch == null)
            {
                return;
            }
            
            IBatchCollision neighborBatch;
            
            do
            {
                neighborBatch = batch.FindNeighborBatch();

                if (neighborBatch != null)
                {
                    blockBatch.MergeWith(neighborBatch.BlockBatch);
                    blockBatch.Dispose();
                    blockBatch = neighborBatch.BlockBatch;
                    block.SetCollider(neighborBatch.BlockBatch);
                }
            } while (neighborBatch != null);
        }
    }
}