﻿using System;
using UnityEngine;

namespace ConstructionSystem
{
    public abstract class BlockMeshBatch : MonoBehaviour
#if BATCH_CONSTRUCTION_SYSTEM
        , IMeshBatch
#endif
    {
        public Material Material { get; protected set; }
        public CombineInstance CombineInstance { get; protected set; }
        public BatchData BatchData { get; set; }

        private IBuildingBlock block;
        private Action<IBuildingBlock> initializeBlockCallback, deleteBlockCallback;

#if BATCH_CONSTRUCTION_SYSTEM

        private Action<IBlockPhysics> simulatingPhysicsChangedCallback;
        private bool batched;
        private Timed.EveryFrameStopper physicsRenderUpdate;
        private Action renderPhysicsMeshCallback;

#endif

        protected virtual void OnEnable()
        {
#if BATCH_CONSTRUCTION_SYSTEM

            if (renderPhysicsMeshCallback == null)
            {
                renderPhysicsMeshCallback = () => { Graphics.DrawMesh(CombineInstance.mesh, Matrix4x4.TRS(transform.position, transform.rotation, transform.lossyScale), Material, ConstructionLayers.BlockPhysicsLayer); };
            }
            
#endif
            
            if (initializeBlockCallback == null)
            {
                initializeBlockCallback = OnInitialize;
            }

            if (deleteBlockCallback == null)
            {
                deleteBlockCallback = OnDelete;
            }
            
            block = GetComponent<IBuildingBlock>();

            block.InitializeEvent -= initializeBlockCallback;
            block.InitializeEvent += initializeBlockCallback;
            block.DeleteEvent -= deleteBlockCallback;
            block.DeleteEvent += deleteBlockCallback;

#if BATCH_CONSTRUCTION_SYSTEM

            if (simulatingPhysicsChangedCallback == null)
            {
                simulatingPhysicsChangedCallback = OnSimulatingPhysicsChanged;
            }
            
            var blockPhysics = block as IBlockPhysics;
            if (blockPhysics != null)
            {
                blockPhysics.SimulatingPhysicsChangedEvent -= simulatingPhysicsChangedCallback;
                blockPhysics.SimulatingPhysicsChangedEvent += simulatingPhysicsChangedCallback;
            }

#endif

            if (block.IsInitialized)
            {
                OnInitialize(block);
            }
        }

        protected virtual void OnDisable()
        {
            block.InitializeEvent -= initializeBlockCallback;
            block.DeleteEvent -= deleteBlockCallback;
            
#if BATCH_CONSTRUCTION_SYSTEM

            var blockPhysics = block as IBlockPhysics;
            if (blockPhysics != null)
            {
                blockPhysics.SimulatingPhysicsChangedEvent -= simulatingPhysicsChangedCallback;
            }
#endif
        }

        public bool AddToBatch()
        {
#if BATCH_CONSTRUCTION_SYSTEM

            if (batched)
            {
                return true;
            }

            if (!TryGatherBatchData())
            {
                return false;
            }

            if (block.Structure == null)
            {
                return false;
            }

            if (block.Structure.MeshBatcher.Add(this))
            {
                batched = true;
            }

            return batched;

#else
            return false;
#endif
        }

        public void RemoveFromBatch()
        {
#if BATCH_CONSTRUCTION_SYSTEM

            if (!batched || block.Structure == null)
            {
                return;
            }

            if (block.Structure.MeshBatcher.Remove(this))
            {
                batched = false;
            }

#endif
        }

        protected virtual void OnInitialize(IBuildingBlock initBlock)
        {
#if BATCH_CONSTRUCTION_SYSTEM
            AddToBatch();
#endif
        }

        protected virtual void OnDelete(IBuildingBlock deleteBlock)
        {
#if BATCH_CONSTRUCTION_SYSTEM
            RemoveFromBatch();
            physicsRenderUpdate.Cancel();
#endif
        }

        protected virtual void OnSimulatingPhysicsChanged(IBlockPhysics blockPhysics)
        {
#if BATCH_CONSTRUCTION_SYSTEM
            if (blockPhysics.SimulatingPhysics && batched)
            {
                RemoveFromBatch();
                physicsRenderUpdate = Timed.EveryFrame(renderPhysicsMeshCallback);
            }
#endif
        }

        protected abstract bool TryGatherBatchData();
    }
}