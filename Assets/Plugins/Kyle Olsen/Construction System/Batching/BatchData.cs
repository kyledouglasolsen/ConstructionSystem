﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions.Must;

namespace ConstructionSystem
{
    public class BatchData : IDisposable
    {
        private const int MaxVertCount = ushort.MaxValue;
        private static readonly PooledQueue<Mesh> MeshPool = new PooledQueue<Mesh>(() => new Mesh());
        private static readonly StaticArrayPool<CombineInstance> CombinePool = new StaticArrayPool<CombineInstance>();

        private int vertCount;
        private GameObject batchObject;
        private Material material;
        private Transform parent;
        private MeshFilter filter;
        private IBatchMeshes meshBatcher;
        private CombineInstance[] combineArray;
        private HashSet<IMeshBatch> batches = new HashSet<IMeshBatch>();

        private MeshFilter Filter
        {
            get
            {
                if (batchObject == null)
                {
                    var mesh = MeshPool.Dequeue();
                    mesh.Clear();

                    batchObject = new GameObject($"{material.name} batch");
                    batchObject.transform.SetParent(parent);
                    batchObject.transform.localPosition = batchObject.transform.localEulerAngles = Vector3.zero;
                    batchObject.transform.localScale = Vector3.one;

                    var renderer = batchObject.AddComponent<MeshRenderer>();
                    renderer.sharedMaterial = material;

                    filter = batchObject.AddComponent<MeshFilter>();
                    filter.sharedMesh = mesh;
                }

                return filter;
            }
        }

        public BatchData(Transform parent, Material material)
        {
            this.parent = parent;
            this.material = material;
        }

        public bool Add(IMeshBatch batch)
        {
            if (!HasRoomForMesh(batch.CombineInstance.mesh))
            {
                return false;
            }

            var added = batches.Add(batch);

            if (added)
            {
                vertCount += batch.CombineInstance.mesh.vertexCount;
                batch.BatchData = this;
            }

            return added;
        }

        public bool Remove(IMeshBatch batch)
        {
            var removed = batches.Remove(batch);

            if (removed && batch.BatchData == this)
            {
                vertCount -= batch.CombineInstance.mesh.vertexCount;
                batch.BatchData = null;
            }

            return removed;
        }

        public void Batch()
        {
            if (batches.Count < 1)
            {
                DestroyBatch();
                return;
            }

            if (combineArray == null)
            {
                combineArray = CombinePool.Get(batches.Count);
            }
            else if (combineArray.Length != batches.Count)
            {
                CombinePool.Return(combineArray);
                combineArray = CombinePool.Get(batches.Count);
            }

            var mesh = Filter.sharedMesh;
            mesh.Clear();

            var i = 0;
            foreach (var batch in batches)
            {
                if (UnityHelpers.IsNull(batch))
                {
                    continue;
                }

                combineArray[i++] = batch.CombineInstance;
            }

            mesh.CombineMeshes(combineArray);
            mesh.RecalculateBounds();
            CombinePool.Return(combineArray);
            combineArray = null;
        }

        public void Dispose()
        {
            DestroyBatch();
        }

        private bool HasRoomForMesh(Mesh newMesh)
        {
            return vertCount + newMesh.vertexCount < MaxVertCount;
        }

        private void DestroyBatch()
        {
            vertCount = 0;

            if (combineArray != null)
            {
                CombinePool.Return(combineArray);
                combineArray = null;
            }

            if (Filter.sharedMesh != null)
            {
                MeshPool.Enqueue(Filter.sharedMesh);
                Filter.sharedMesh = null;
            }

            if (batchObject != null)
            {
                UnityEngine.Object.Destroy(batchObject);
            }
        }
    }
}