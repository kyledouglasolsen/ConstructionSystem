﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ConstructionSystem
{
    public class StructureMeshBatcher : IBatchMeshes, IDisposable
    {
        private Transform structureTransform;
        private bool dirty;
        private Action updateBatchCallback;
        private readonly HashSet<Material> dirtyMaterials = new HashSet<Material>();
        private readonly Dictionary<Material, List<BatchData>> batchLookup = new Dictionary<Material, List<BatchData>>();

        public StructureMeshBatcher(IBuildingStructure structure)
        {
            structureTransform = ((Component)structure).transform;
            updateBatchCallback = UpdateBatch;
        }

        public bool Add(IMeshBatch batch)
        {
            if (batch?.Material == null)
            {
                return false;
            }

            var addedbatch = AddToBatch(batch);

            if (addedbatch != null)
            {
                dirtyMaterials.Add(batch.Material);
                TryMarkForUpdate();

                return true;
            }

            return false;
        }

        public bool Remove(IMeshBatch batch)
        {
            if (batch?.Material == null)
            {
                return false;
            }

            var removed = batch.BatchData.Remove(batch);

            if (removed)
            {
                dirtyMaterials.Add(batch.Material);
                TryMarkForUpdate();

                return true;
            }

            return false;
        }

        public void Dispose()
        {
            foreach (var kvp in batchLookup)
            {
                foreach (var batch in kvp.Value)
                {
                    batch?.Dispose();
                }
            }

            dirtyMaterials.Clear();
            batchLookup.Clear();
        }

        private void UpdateBatch()
        {
            try
            {
                foreach (var material in dirtyMaterials)
                {
                    var batchList = batchLookup[material];

                    foreach (var batch in batchList)
                    {
                        batch?.Batch();
                    }
                }

                dirtyMaterials.Clear();
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
            finally
            {
                dirty = false;
            }
        }

        private void TryMarkForUpdate()
        {
            if (!dirty)
            {
                Timed.NextFrame(updateBatchCallback);
                dirty = true;
            }
        }

        private BatchData AddToBatch(IMeshBatch meshBatch)
        {
            var batchDataList = GetBatchList(meshBatch.Material);

            for (var i = 0; i < batchDataList.Count; ++i)
            {
                if (batchDataList[i].Add(meshBatch))
                {
                    return batchDataList[i];
                }
            }

            var newData = new BatchData(structureTransform, meshBatch.Material);
            newData.Add(meshBatch);
            batchDataList.Add(newData);

            return newData;
        }

        private List<BatchData> GetBatchList(Material material)
        {
            List<BatchData> batchDataList;

            if (!batchLookup.TryGetValue(material, out batchDataList))
            {
                batchDataList = new List<BatchData>();
                batchLookup[material] = batchDataList;
            }

            return batchDataList;
        }
    }
}