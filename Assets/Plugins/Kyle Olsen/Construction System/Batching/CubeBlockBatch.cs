﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ConstructionSystem
{
    public class CubeBlockBatch : BlockBatch<IBuildingBlock>
    {
        public CubeBlockBatch(IBuildingBlock block, Quaternion rotation) : base(block, rotation)
        {
        }

        public CubeBlockBatch(FastList<IBuildingBlock> blocks, Quaternion rotation) : base(blocks, rotation)
        {
        }

        public override bool TestForCompatibility<TS>(IBuildingBlock block)
        {
            if (block.IsPreview)
            {
                return false;
            }

            var blockComponent = block as Component;
            if (blockComponent != null && !blockComponent.gameObject.activeInHierarchy)
            {
                return false;
            }

            TS otherBlock;

            try
            {
                otherBlock = (TS)block;
            }
            catch (InvalidCastException)
            {
                return false;
            }

            Bounds otherBounds;

            if (otherBlock.IsBatched)
            {
                var otherBlockBatch = otherBlock.BlockBatch;

                if (ReferenceEquals(this, otherBlockBatch))
                {
                    return false;
                }

                otherBounds = otherBlockBatch.Bounds;
            }
            else
            {
                otherBounds = otherBlock.Bounds;
            }

            var distanceToOtherBlock = (otherBounds.center - Bounds.center);
            var directionToOtherBlock = distanceToOtherBlock.normalized;
            var forwardDot = Mathf.Abs(Vector3.Dot(Vector3.forward, directionToOtherBlock));
            var rightDot = Mathf.Abs(Vector3.Dot(Vector3.right, directionToOtherBlock));
            var upDot = Mathf.Abs(Vector3.Dot(Vector3.up, directionToOtherBlock));
            var validForward = Mathf.Approximately(forwardDot, 1f);
            var validRight = Mathf.Approximately(rightDot, 1f);
            var validUp = Mathf.Approximately(upDot, 1f);

            if (validForward)
            {
                if (Mathf.Abs(distanceToOtherBlock.magnitude - Bounds.size.z) > Epsilon)
                {
                    return false;
                }

                return Mathf.Abs(otherBounds.size.x - Bounds.size.x) <= Epsilon && Mathf.Abs(Bounds.max.y - otherBounds.max.y) <= Epsilon && Mathf.Abs(Bounds.min.y - otherBounds.min.y) <= Epsilon;
            }

            if (validRight)
            {
                if (Mathf.Abs(distanceToOtherBlock.magnitude - Bounds.size.x) > Epsilon)
                {
                    return false;
                }

                return Mathf.Abs(otherBounds.size.z - Bounds.size.z) <= Epsilon && Mathf.Abs(Bounds.max.y - otherBounds.max.y) <= Epsilon && Mathf.Abs(Bounds.min.y - otherBounds.min.y) <= Epsilon;
            }

            if (validUp)
            {
                if (Mathf.Abs(distanceToOtherBlock.magnitude - Bounds.size.y) > Epsilon)
                {
                    return false;
                }

                return Mathf.Abs(otherBounds.size.x - Bounds.size.x) <= Epsilon && Mathf.Abs(otherBounds.size.z - Bounds.size.z) <= Epsilon;
            }

            return false;
        }

        public override IBlockBatch<IBuildingBlock> CreateNewBatch(FastList<IBuildingBlock> blocks, Quaternion rotation)
        {
            return new CubeBlockBatch(blocks, rotation);
        }
    }
}