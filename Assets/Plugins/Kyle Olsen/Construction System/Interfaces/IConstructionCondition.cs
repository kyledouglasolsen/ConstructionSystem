﻿namespace ConstructionSystem.Conditions
{
    public partial interface IConstructionCondition
    {
        bool Evaluate(IBuildingBlock block);
    }
}
