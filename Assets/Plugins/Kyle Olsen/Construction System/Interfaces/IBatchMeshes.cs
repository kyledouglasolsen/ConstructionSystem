﻿namespace ConstructionSystem
{
    public interface IBatchMeshes
    {
        bool Add(IMeshBatch batch);
        bool Remove(IMeshBatch batch);
    }
}