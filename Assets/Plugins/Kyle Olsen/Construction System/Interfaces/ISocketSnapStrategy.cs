﻿using UnityEngine;

namespace ConstructionSystem.BuildingSockets
{
    public interface ISocketSnapStrategy
    {
        bool Snap(IBuildingBlock block, IBuildingSocket targetSocket, Vector3 buildPosition, Quaternion buildRotation);
    }
}