﻿using UnityEngine;

public interface IConstructionCollider
{
    Collider this[int index] { get; }
    int Count { get; }

    Collider Main();
    void ReturnToCache();
}