﻿using System.Collections.Generic;

namespace ConstructionSystem.BuildingSockets
{
    public interface IActiveSocketCollection : IEnumerable<IBuildingSocket>
    {
        void Add(IBuildingSocket socket);
        void Remove(IBuildingSocket socket);
        bool Contains(IBuildingSocket socket);
        void DeleteAll();
        IBuildingSocket GetSocketById(uint id);
    }
}