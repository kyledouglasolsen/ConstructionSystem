﻿using UnityEngine;

namespace ConstructionSystem
{
    public interface IMeshBatch
    {
        BatchData BatchData { get; set; }
        
        Material Material { get; }
        CombineInstance CombineInstance { get; }
    }
}