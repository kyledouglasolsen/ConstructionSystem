﻿using System;
using ConstructionSystem.BuildingSockets;
using ConstructionSystem.Conditions;

namespace ConstructionSystem
{
    public interface IBuildingBlock : ITransform, IHasBounds, IDeepCopy<IBuildingBlock>
    {
        bool IsInitialized { get; }
        bool IsPreview { get; }
        IBuildingStructure Structure { get; }
        IBuildingBlockData Data { get; }
        ISocketCollection Sockets { get; }
        IConditionCollection Conditions { get; }
        IActiveSocketCollection ActiveSockets { get; }
        IConstructionCollider Collider { get; }

        event Action<IBuildingBlock> InitializeEvent;
        event Action<IBuildingBlock> DeleteEvent;

        void Initialize(bool isPreview);
        void SetStructure(IBuildingStructure structure);
        void SetCollider(IConstructionCollider constructionCollider);
        bool EvaluateConditions();
        bool EvaluateConditions(out IConstructionCondition failedCondition);
        void Delete(bool destroyGo);

        Type GetTargetSocketType();
    }
}