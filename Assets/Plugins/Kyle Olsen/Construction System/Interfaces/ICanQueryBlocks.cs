﻿using System.Collections.Generic;
using UnityEngine;

namespace ConstructionSystem
{
    public interface ICanQueryBlocks
    {
        IBuildingBlock GetBlockFromPoint(Vector3 point);
        void OverlapBoxNonAlloc(Bounds testBounds, ref List<IBuildingBlock> cachedOverlapBlocks, float scaleSize = 1f);
    }

    public interface ICanQueryBlocks<out T> where T : IBuildingBlock
    {
        T GetBlockFromPoint(Vector3 point);
        void OverlapBoxNonAlloc(Bounds testBounds, ref List<IBuildingBlock> cachedOverlapBlocks, float scaleSize = 1f);
    }
}