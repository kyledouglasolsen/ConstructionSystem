﻿namespace ConstructionSystem
{
    public interface IBuildingBlockCreator
    {
        bool TrySpawn(IBuildingBlock block, IBuildingBlockPlacer placer);
    }
}