﻿#if UNITY_EDITOR
namespace ConstructionSystem.BuildingSockets
{
    public partial interface IBuildingSocketData
    {
        void DrawGizmos(BaseBlock baseBlock);
    }
}
#endif