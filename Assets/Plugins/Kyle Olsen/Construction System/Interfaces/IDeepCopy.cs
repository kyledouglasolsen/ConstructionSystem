﻿namespace ConstructionSystem
{
    public interface IDeepCopy<out T>
    {
        T Copy();
    }
}
