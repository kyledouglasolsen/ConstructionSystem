﻿using UnityEngine;

namespace ConstructionSystem
{
    public interface IBuildingBlockData
    {
        IColliderData ColliderData { get; }
        
        bool RequireSnap { get; }
        Vector3 BuildPositionOffset { get; }
        Vector3 BuildRotationOffset { get; }

#if UNITY_EDITOR
        void DrawGizmos(IBuildingBlock block);
#endif
    }
}