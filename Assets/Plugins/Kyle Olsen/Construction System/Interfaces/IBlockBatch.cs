﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ConstructionSystem
{
    public interface IBlockBatch<out T> : ICanQueryBlocks<IBuildingBlock>, IConstructionCollider, IHasBounds, IDisposable where T : IBuildingBlock
    {
        FastList<IBuildingBlock> Blocks { get; }

        bool TestForCompatibility<TS>(IBuildingBlock block) where TS : IBatchCollision, IHasBounds;
        bool Add(IBuildingBlock block, bool updateCollider);
        bool Remove(IBuildingBlock block, bool updateCollider);
        bool Contains(IBuildingBlock block);

        IBlockBatch<T> CreateNewBatch(FastList<IBuildingBlock> newBlocks, Quaternion rotation);
        void MergeWith(IBlockBatch<IBuildingBlock> otherBatch);

        void UpdateCollider();
        void Rebatch();
    }
}