﻿using UnityEngine;

namespace ConstructionSystem.BuildingSockets
{
    public interface IBuildingSocket : ITransform
    {
        bool Enabled { get; set; }

        uint SocketId { get; }
        IBuildingBlock OwningBlock { get; }
        IBuildingSocketData Data { get; }
        ISocketSnapStrategy SocketSnapStrategy { get; }
        IConstructionCollider ConstructionCollider { get; }

        void SetSocketId(uint socketId);
        void SetData(IBuildingBlock block, IBuildingSocketData data, bool isPreview);
        bool IsValidConnection(IBuildingSocket otherSocket);
        bool IsValidConnection(IBuildingBlock block);
        bool SnapBlockTo(IBuildingBlock block, Vector3 buildPosition, Quaternion buildRotation);
        void Delete();
    }
}