﻿using System.Collections.Generic;

namespace ConstructionSystem.BuildingSockets
{
    public interface ISocketCollection : IEnumerable<IBuildingSocketData>
    {
        IBuildingSocketData[] Sockets { get; }

        bool Contains(IBuildingSocketData socketData);
    }
}