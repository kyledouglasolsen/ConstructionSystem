﻿#if UNITY_EDITOR
namespace ConstructionSystem.Conditions
{
    public partial interface IConstructionCondition
    {
        void OnValidate();
        void DrawGizmos(IBuildingBlock block);
    }
}
#endif
