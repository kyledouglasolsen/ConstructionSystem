﻿using System;

namespace ConstructionSystem
{
    public interface IBlockPhysics
    {
        event Action<IBlockPhysics> SimulatingPhysicsChangedEvent;
        bool SimulatingPhysics { get; }

        void SetSimulatingPhysics(bool newSimulatingPhysics);
    }
}