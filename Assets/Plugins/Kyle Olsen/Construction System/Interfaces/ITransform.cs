﻿using UnityEngine;

namespace ConstructionSystem
{
    public interface ITransform
    {
        Vector3 Position { get; set; }
        Quaternion Rotation { get; set; }
        Vector3 Up { get; }
        Vector3 Forward { get; }
        Vector3 Right { get; }

        void SetParent(ITransform newParent);
        Vector3 TransformPoint(Vector3 position);
        Vector3 InverseTransformPoint(Vector3 position);
        Vector3 TransformDirection(Vector3 direction);
        Vector3 InverseTransformDirection(Vector3 direction);
        void SetPositionAndRotation(Vector3 position, Quaternion rotation);
    }
}
