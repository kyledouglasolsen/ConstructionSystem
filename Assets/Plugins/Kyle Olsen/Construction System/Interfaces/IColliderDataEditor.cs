﻿using UnityEngine;

#if UNITY_EDITOR
namespace ConstructionSystem
{
    public partial interface IColliderData
    {
        void DrawGizmos(ITransform transform, Vector3 positionOffset, Vector3 rotationOffset, bool drawCenter = true);
    }
}
#endif