﻿using UnityEngine;

namespace ConstructionSystem.BuildingSockets
{
    public partial interface IBuildingSocketData
    {
        IColliderData ColliderData { get; }
        Vector3 LocalPositionOffset { get; }
        Vector3 LocalRotationOffset { get; }

        IBuildingSocket CreateSocket(IBuildingBlock block, bool isPreview);
        IBuildingSocket InitializeSocket(IBuildingBlock block, IBuildingSocket socket, bool isPreview);
    }
}