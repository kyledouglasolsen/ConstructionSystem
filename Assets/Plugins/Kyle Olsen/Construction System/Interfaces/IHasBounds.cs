﻿using UnityEngine;

namespace ConstructionSystem
{
    public interface IHasBounds
    {
        Bounds Bounds { get; }
        
        void ForceUpdateBounds();
    }
}