﻿using UnityEngine;

namespace ConstructionSystem
{
    public interface IBuildingBlockPlacer
    {
        IBuildingBlock PreviewBlock { get; }
        SocketFinder.FoundSocket SnappingSocketData { get; }
        Vector3 BuildPosition { get; }
        Quaternion BuildRotation { get; }
        bool HasValidBuildLocation { get; }

        void SetHasValidBuildLocation(bool hasValidBuildLocation);
        void SetPosition(Vector3 position);
        void SetRotation(Quaternion rotation);
        bool Cast();
        void SetPreviewBlock(IBuildingBlock previewBlock);
        void DeletePreviewBlock();
    }
}