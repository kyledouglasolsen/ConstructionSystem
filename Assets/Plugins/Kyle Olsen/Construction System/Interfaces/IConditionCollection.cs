﻿using System.Collections.Generic;

namespace ConstructionSystem.Conditions
{
    public interface IConditionCollection : IEnumerable<IConstructionCondition>
    {
        bool Evaluate(IBuildingBlock block);
        bool Evaluate(IBuildingBlock block, out IConstructionCondition failedCondition);
    }
}