﻿namespace ConstructionSystem
{
    public interface IBatchCollision
    {
        bool IsBatched { get; }
        IBlockBatch<IBuildingBlock> BlockBatch { get; }

        IBatchCollision FindNeighborBatch();
        IBlockBatch<IBuildingBlock> CreateNewBatch();
    }
}