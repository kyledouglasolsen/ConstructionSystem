﻿using UnityEngine;

namespace ConstructionSystem
{
    public partial interface IColliderData
    {
        bool IsTrigger { get; }
        Vector3 Center { get; }
        Vector3 Size { get; }

        IConstructionCollider ApplyTo(IBuildingBlock block);
        IConstructionCollider ApplyTo(Component component);
        IConstructionCollider ApplyTo(GameObject gameObject);
    }
}