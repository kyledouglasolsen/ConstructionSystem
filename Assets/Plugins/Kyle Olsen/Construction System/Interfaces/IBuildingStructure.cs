﻿using System.Collections.Generic;

namespace ConstructionSystem
{
    public interface IBuildingStructure : ITransform, ICanQueryBlocks, IEnumerable<IBuildingBlock>
    {
        uint StructureId { get; }
        void SetStructureId(uint structureId);

        IBatchMeshes MeshBatcher { get; }

        bool AddBlock(IBuildingBlock block);
        bool RemoveBlock(IBuildingBlock block);
        bool UpdateEvaluationList();

        void EvaluateStabilityFrom(IBuildingBlock block);
        IBatchCollision FindNeighborBatch<T>(IBuildingBlock block) where T : IBatchCollision, IHasBounds;
    }
}