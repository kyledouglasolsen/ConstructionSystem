﻿using UnityEngine;

namespace ConstructionSystem
{
    public interface IMeshColliderData : IColliderData
    {
        Mesh Mesh { get; }
        Vector3 LocalPositionOffset { get; }
        Vector3 LocalRotationOffset { get; }
    }
}

