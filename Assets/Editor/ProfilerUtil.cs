﻿using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using UnityEditor;
using UnityEditorInternal;

public class ProfilerUtil
{
	private const string HtmlOutputPath = "profiler.html";

	[MenuItem("Tools/Dump selected profiler frame to HTML")]
	public static void DumpProfilerFrame()
	{
		var sb = new StringBuilder();

		var property = new ProfilerProperty();
		property.SetRoot(GetSelectedFrame(), ProfilerColumn.TotalPercent, ProfilerViewType.Hierarchy);
		property.onlyShowGPUSamples = false;

		sb.Append(@"<html>
                    <head>
                    <title>Unity Profiler Data</title>
                    <style type=""text/css"">
                    html, body {
                    font-family: Helvetica, Arial, sans-serif;
                    }
                    table {
                    width: 100%;
                    border-collapse: collapse;
                    }
                    th:first-child, td:first-child {
                    text-align: left;
                    }
                    th:not(:first-child), td:not(:first-child) {
                    text-align: right;
                    }
                    tbody tr:nth-child(odd) {
                    background-color: #EEE;
                    }
                    th, td {
                    margin: 0;
                    padding: 5px;
                    }
                    th {
                    padding-bottom: 10px;
                    }
                    td {
                    font-size: 12px;
                    }
                    </style>
                    </head>
                    <body>
                    <table>
                    <thead>
                    <tr><th>Path</th><th>Total</th><th>Self</th><th>Calls</th><th>GC Alloc</th><th>Total ms</th><th>Self ms</th></tr>
                    </thead>
                    <tbody>");

		while (property.Next(true))
		{
			sb.AppendLine($"<td style=\"padding-left: {property.depth * 10}px\">{property.GetColumn(ProfilerColumn.FunctionName)}</td>");
			sb.AppendLine($"<td>{property.GetColumn(ProfilerColumn.TotalPercent)}</td>");
			sb.AppendLine($"<td>{property.GetColumn(ProfilerColumn.SelfPercent)}</td>");
			sb.AppendLine($"<td>{property.GetColumn(ProfilerColumn.Calls)}</td>");
			sb.AppendLine($"<td>{property.GetColumn(ProfilerColumn.GCMemory)}</td>");
			sb.AppendLine($"<td>{property.GetColumn(ProfilerColumn.TotalTime)}</td>");
			sb.AppendLine($"<td>{property.GetColumn(ProfilerColumn.SelfTime)}</td>");
			sb.AppendLine("</tr>");
		}

		sb.AppendLine(@"</tbody>
                        </table>
                        </body>
                        </html>");

		new Thread(() =>
		{
			if (File.Exists(HtmlOutputPath)) File.Delete(HtmlOutputPath);

			using (var stream = File.OpenWrite(HtmlOutputPath))
			{
				using (var writer = new StreamWriter(stream))
				{
					writer.Write(sb.ToString());
					writer.Close();
				}
			}
		}).Start();
	}

	private static int GetSelectedFrame()
	{
		var editorAssembly = Assembly.GetAssembly(typeof(EditorApplication));
		var profilerWindowType = editorAssembly.GetType("UnityEditor.ProfilerWindow");
		var profilerWindowsField = profilerWindowType.GetField("m_ProfilerWindows", BindingFlags.NonPublic | BindingFlags.Static);
		var firstProfilerWindow = ((System.Collections.IList)profilerWindowsField.GetValue(null))[0];
		var getFrameMethod = profilerWindowType.GetMethod("GetActiveVisibleFrameIndex");
		return (int)getFrameMethod.Invoke(firstProfilerWindow, null);
	}
}